#JAKARTA ANT TASK FOR NOWEB

AntWeave is an implementation of the NoWeb literate-programming tool originally designed by Norman Ramsey.
Because it uses Java (and the XML/XSL environment) it is universal to systems running Java. There are some
minor differences and assumptions that this implementation uses compared to the original NoWeb. These are
only in how the system is used, rather than the source files.
         
##Installation instructions

The tar file `AntWeave-x.x.tar` contains the following

````
AntWeave-x.x.tar
    |-------- AntWeave.jar
    |-------- README
    |-------- docs.pdf
    \-------- antweave -------- addXRef.xsl
                           |--- filters.xml
                           |--- index.xsl
                           |--- tangle.xsl
                           |--- types.xml
                           \--- weave.xsl
````
                        
Place `AntWeave.jar` and the `antweave` folder into your Java extension folder
(for example `/Library/Java/Extensions/` on Mac OS-X).

Assuming you already have Ant that is all. A typical build file would be

````
<project name="test" basedir=".">
  
   <taskdef name="antweave" classname="uk.org.hsfr.antweave.AntWeave"/>
  
   <property name="sourceDir" value="./convertInteger"/>
  
   <target name="weave">
      <antweave srcdir="${sourceDir}" action="weave">
         <include name="convertInteger.nw"/>
      </antweave>
   </target>
  
   <target name="tangle">
      <antweave srcdir="${sourceDir}" root="convertInteger.m">
         <include name="convertInteger.nw"/>
      </antweave>
   </target>
  
   <target name="html">
      <antweave srcdir="${sourceDir}" filters="filters/filters-user.xml" tofile="convertInteger.html" action="html">
         <include name="convertInteger.nw"/>
      </antweave>
   </target>

</project>
````
      
See the documentation (docs.pdf) for possible combinations of attributes etc
(hint: it works a lot like the *javac* task).

Any questions, bug-reports or suggestions to me,
Hugh Field-Richards <hsfr@hsfr.org.uk>. It is a work in progress but seems
stable enough for most that I have thrown at it. It is easily extensible
(XSL experience essential) and any contributions would be welcome.

