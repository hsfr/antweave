package uk.org.hsfr.antweave;

import java.io.File;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.BufferedReader;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PipedWriter;
import java.io.InputStream;

import java.util.Enumeration;
import java.util.Random;
import java.util.Map;
import java.util.Hashtable;
import java.util.ListIterator;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.dom.DOMSource;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;

import java.net.URL;

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
// Filters.java
//
// Author
// ------
// Name  : Hugh Field-Richards.
// Email : hsfr@hsfr.org.uk
//
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

class Filters extends DefaultHandler
{
    /** The list of overriding actions */
    private String userFilterFileName = null;
    
    /** An ordered list of filters (as XSLT file names) for each action that make up a single pipeline */
    protected ArrayList<String> filtersList = null;
    
    /** The list of pipelines stored as a table of pipeline filters keyed by the pipeline name */
    protected Hashtable<String, ArrayList<String>> pipelineList = new Hashtable<String, ArrayList<String>>();
    
    /** Internal list of actions accumulated */
    private String currentPipelineList = "";
    
    /** Directory where filters are stored */
    private String extensionDirectory = "";
    
    /** Private reference to the default handler instance */
    private Filters contentHandler = null;
    
    /** Internal state for reading the XML file */
    private XMLParseState parsingState = XMLParseState.ROOT;
    
    public enum XMLParseState {
        ROOT,
        PIPELINE,
        ACTION
    }
    
    // The main instance of this class creates an instance of itself to handle the SAX events. It is this latter
    // instance that stores the lists of pipelines and filters. Care must be taken when accessing the main instance
    // to gather data from there.
    
    private String whichInstance = "";
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * This is constructor when this class is used as the event handler for the Sax parser.
     */
    
    public Filters()
    {
        this.whichInstance = "SAX handler";
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Constructor to input the filters information.
     *
     * @param userFilterFile The file definition of the user defined filters (null if nothing defined)
     * @throws Exception if problem
     */
    
    public Filters( File userFilterFile ) throws Exception
    {
        try {
            this.whichInstance = "Filter handler";
            boolean found = false;
            
            // Gather all the folders where there may be extensions
            String javaExtDir = Constants.LOCAL_JAVA_EXTENSIONS_FOLDERS;
            // Folders are colon separated list so split into a series of Srings in an array
            String[] extensionDir = javaExtDir.split( ":" );
            String filterFileName = "";
            // Set up filters list ready for adding from file
            this.filtersList = new ArrayList<String>();
            
            // Input the default filters file which we may override with the users one.
            // Search for the default filter which should be in same place
            // as user filters: "{java_ext_directory}/antweave"
            for ( int i = 0; i < extensionDir.length; i++ ) {
                String dir = String.format( "%s%santweave", extensionDir[i], Constants.PATH_SEPARATOR );
                // System.out.printf( "dir = '%s'\n", dir );
                filterFileName = String.format( "%s%s%s", dir, Constants.PATH_SEPARATOR, Constants.DEFAULT_FILTER_FILE_NAME );
                // System.out.printf( "file = '%s'\n", filterFileName );

                File f = new File( filterFileName );
                if ( f.exists() && !f.isDirectory() ) {
                    this.extensionDirectory = dir;
                    found = true;
                    // System.out.printf( "extensionDir[%d] = '%s'\n", i, this.extensionDirectory );
                    break;
                }
            }
            if ( !found ) {
                throw new Exception( "Cannot find default filter in any Java extensions folder" );
            }
            // System.out.printf( "filterFileName = '%s'\n", filterFileName );
            // Set up default filter file stream
            FileInputStream fis = new FileInputStream( filterFileName );
            InputStream defaultFilterStream = fis;
            
            // InputStream defaultFilterStream = getClass().getResourceAsStream( Constants.DEFAULT_FILTER_FILE_NAME );
            // this.userFilterFileName = userFilterFileName;
            
            // Set up the SAX parser factory
            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setNamespaceAware( true );
            SAXParser saxParser = spf.newSAXParser();
            
            XMLReader xmlReader = saxParser.getXMLReader();
            this.contentHandler = new Filters();
            xmlReader.setContentHandler( this.contentHandler );
            xmlReader.parse( new InputSource( new BufferedInputStream( defaultFilterStream ) ) );
            
            // Now transfer all the data gathered by the handler into the main instance. We can do this
            // quite safely because the user and default filter lists are both read only once at
            // startup. We never update them dynamically.
            
            // Go through the list and add the full file path. This is done so that there can be a mixture of
            // default and user filters. If this was not done the user would have to copy all the default
            // filters into the user filter folder.
            
            this.addAbsolutePathToFilters( this.extensionDirectory );
            this.filtersList = this.contentHandler.filtersList;
            this.pipelineList = this.contentHandler.pipelineList;
            // System.out.printf( "===============\n%s\n", this.toString() );

            found = false;
            // At this point we have the default filter set in this.filtersList and this.pipelineList
            // Has a user filter file been given?
            if ( userFilterFile != null ) {
                // Yes - so is there an associated path with this file
                filterFileName = userFilterFile.getPath();
                //System.out.printf( "filterFileName = '%s'\n", filterFileName );
                String userFilterDir = filterFileName.substring(0,filterFileName.lastIndexOf(File.separator) );
                //System.out.printf( "userFilterDir = '%s'\n", userFilterDir );
                // Absolute path or one relative to where this is being run
                if ( !userFilterDir.startsWith( File.separator ) ) {
                    // Relative path so add the current directory at the front
                    filterFileName = String.format( "%s%s%s%s%s", System.getProperty("user.dir"), Constants.PATH_SEPARATOR, userFilterDir, Constants.PATH_SEPARATOR, userFilterFile.getName() );
                }
                // At this point we should have a fully resolved filter file name
                // Check if it exists ...
                File uff = new File( filterFileName );
                if ( uff.exists() && !uff.isDirectory() ) {
                    // Great, it exists, so use that. We assume that the filters are in the same folder as the filter definition file
                    this.extensionDirectory = filterFileName.substring(0,filterFileName.lastIndexOf(File.separator) );
                    found = true;
                }
            }
            if ( !found ) {
                // Not found so we exit
                // System.out.printf( "found = '%s'\n", found );
                return;
            }
            // System.out.printf( "user filter definition = '%s'\n", filterFileName );

            // Set up user's filter file stream
            FileInputStream ufis = new FileInputStream( filterFileName );
            InputStream usersFilterStream = ufis;
            
            // Set up another SAX parser factory (not sure whether it is safe to use the existing one - will check later)
            spf = SAXParserFactory.newInstance();
            spf.setNamespaceAware( true );
            saxParser = spf.newSAXParser();
            
            xmlReader = saxParser.getXMLReader();
            this.contentHandler = new Filters();
            xmlReader.setContentHandler( this.contentHandler );
            xmlReader.parse( new InputSource( new BufferedInputStream( usersFilterStream ) ) );
            
            // Now transfer all the data gathered by the handler into the main instance. We can do this
            // quite safely because the user and default filter lists are both read only once at
            // startup. We never update them dynamically.
            // this.filtersList = this.contentHandler.filtersList;
            // this.pipelineList = this.contentHandler.pipelineList;
            this.addAbsolutePathToFilters( this.extensionDirectory );
            // System.out.printf( "%s\n", this.contentHandler.toString() );
            // Merge the two lists together
            this.mergeUserToDefaultFilterList();
            // System.out.printf( "%s\n", this.toString() );
        } catch( Exception e ) {
            System.out.println( "Error creating Filters: " + e );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the location of the filters and XSL files.
     *
     * @return location path as string
     * @throws if directory does not exist
     */
    
    public static String getDefaultFiltersLocation() throws Exception
    {
        // Gather all the folders where there may be extensions
        String javaExtDir = Constants.LOCAL_JAVA_EXTENSIONS_FOLDERS;
        // System.out.printf( "javaExtDir: %s\n", javaExtDir );
        // Folders are colon separated list so split into a series of Strings in an array
        String[] extensionDir = javaExtDir.split( ":" );
        String filterFileName = "";
        String extensionDirectory = "";
        boolean found = false;
        
        // Input the default filters file which we may override with the users one.
        // Search for the default filter which should be in same place
        // as user filters: "{java_ext_directory}/antweave"
        for ( int i = 0; i < extensionDir.length; i++ ) {
            String dir = String.format( "%s%santweave", extensionDir[i], Constants.PATH_SEPARATOR );
            // System.out.printf( "dir = '%s'\n", dir );
            filterFileName = String.format( "%s%s%s", dir, Constants.PATH_SEPARATOR, Constants.DEFAULT_FILTER_FILE_NAME );
            // System.out.printf( "file = '%s'\n", file );
            File f = new File( filterFileName );
            if ( f.exists() && !f.isDirectory() ) {
                extensionDirectory = dir;
                found = true;
                // System.out.printf( "extensionDir[%d] = '%s'\n", i, this.extensionDirectory );
                break;
            }
        }
        if ( !found ) {
            throw new Exception( "Cannot find default filter in any Java extensions folder" );
        }
        return extensionDirectory;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Adds absolute path to each filter name.
     *
     * @param dirPath the absolute path to add
     */
    
    public void addAbsolutePathToFilters( String dirPath )
    {
        String pipelineName = "";
        String fullFilterName = "";
        ArrayList<String> filterList = null;
        // Go through each pipeline entry in the main instance and get the filter list
        for ( ListIterator<String> p = this.contentHandler.pipelineList().listIterator(); p.hasNext(); ) {
            pipelineName = p.next();
            filterList = this.contentHandler.filterList( pipelineName );
            // For each entry in the filterList we add the path
            int idx = 0;
            for ( ListIterator<String> f = filterList.listIterator(); f.hasNext(); ) {
                fullFilterName = String.format( "%s%s%s", dirPath, Constants.PATH_SEPARATOR, f.next() );
                // System.out.println( String.format( "fullFilterName: %s", fullFilterName ) );
                this.contentHandler.filterList( pipelineName ).set( idx++, fullFilterName );
            }
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Adds the users filters to the default list.
     */
    
    public void mergeUserToDefaultFilterList()
    {
        // this.pipelineList and this.filterList are the existing default values.
        // this.contentHandler.pipelineList and this.contentHandler.filterList are the user values.
        //
        // We can replace or add a complete pipeline entry rather than change down to filter entries.
        // By the time we reach here the full path name has been set.
        //
        String pipelineName = "";
        String fullFilterName = "";
        ArrayList<String> newFilterList = null;
        // Go through each pipeline entry and get the filter list
        for ( ListIterator<String> p = this.contentHandler.pipelineList().listIterator(); p.hasNext(); ) {
            pipelineName = p.next();
            // System.out.println( String.format( "pipelineName: %s", pipelineName ) );
            newFilterList = this.contentHandler.filterList( pipelineName );
            this.pipelineList.put( pipelineName, newFilterList );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the full path and file name of the default filter.
     *
     * @return name as string
     */
    
    public String getDefaultFilterFullName()
    {
        return String.format( "%s/%s", this.extensionDirectory, Constants.DEFAULT_FILTER_FILE_NAME );
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     *  Does the the pipeline list contain a particular named pipeline.
     *
     * @param name The name of the pipeline
     * @return True if the named pipeline is in the list, otherwise false
     */
    
    public boolean containsPipeline( String name )
    {
        //System.out.printf( "%s: containsPipeline( %s )\n", this.whichInstance, name );
        return this.pipelineList.containsKey( name );
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     *  Get the number of pipelines found
     *
     * @return number of pipelines
     */
    
    public int numberOfPipelines()
    {
        return this.pipelineList.size();
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     *  Get the number of filters in a named pipeline
     *
     * @param name The name of the pipeline
     * @return number of filters in the pipeline
     */
    
    public int numberOfFilters( String name )
    {
        if ( this.containsPipeline( name ) ) {
            return this.pipelineList.get( name ).size();
        }
        return 0;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get the list of pipelines
     *
     * We have to do it like this because we cannot access the list directly. The
     * handler instance stores this, not the main instance of Filters.
     *
     * @return a list of the pipelines as an array of strings
     */
    
    public ArrayList<String> pipelineList()
    {
        ArrayList<String> list = new ArrayList<String>();
        for ( Enumeration<String> e = this.pipelineList.keys(); e.hasMoreElements(); ) {
            list.add( e.nextElement() );
        }
        return list;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get the list of filters in a named pipeline
     *
     * We have to do this because we cannot access the list directly because the
     * handler instance stores this, not the main instance of Filters.
     *
     * @param name The name of the pipeline
     * @return a list of the filters in a named pipeline as an array of strings
     */
    
    public ArrayList<String> filterList( String name )
    {
        //System.out.printf( "%s: filterList( %s )\n", this.whichInstance, name );
        if ( this.containsPipeline( name ) ) {
            //System.out.printf( "%s:   found %s\n", this.whichInstance, name );
            return this.pipelineList.get( name );
        }
        return null;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    
    public String toString()
    {
        String mess = "";
        ArrayList<String> pipelineList = this.pipelineList();
        ArrayList<String> filterList = null;
        String pipelineName = "";
        String filterName = "";
        for ( ListIterator<String> p = pipelineList.listIterator(); p.hasNext(); ) {
            pipelineName = p.next();
            mess += String.format( "Pipeline: %s\n", pipelineName );
            filterList = this.filterList( pipelineName );
            for ( ListIterator<String> f = filterList.listIterator(); f.hasNext(); ) {
                filterName = f.next();
                mess += String.format( "    %s\n", filterName );
            }
        }
        return mess;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*- SAX Interface Methods -*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * <?xml version="1.0" encoding="UTF-8"?>
     * <antweave>
     *    <pipeline name="tangle">
     *       <filter name="tangle.xsl"/>
     *    </pipeline>
     *    <pipeline name="weave">
     *       <filter name="index.xsl"/>
     *       <filter name="weave.xsl"/>
     *    </pipeline>
     * </antweave>
     */
    
    public void startDocument() throws SAXException {
        // Initialise the tables and lists is done elsewhere (we are
        // reading more than one file).
        this.parsingState = XMLParseState.ROOT;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     */
    
    public void startElement( String namespaceURI, String localName, String qName, Attributes atts ) throws SAXException
    {
        // System.out.printf( "Start element: %s\n", localName );
        if ( localName.equals( Constants.FILTER_ROOT ) && this.parsingState == XMLParseState.ROOT ) {
            this.parsingState = XMLParseState.PIPELINE;
        } else if ( localName.equals( Constants.FILTER_PIPELINE ) ) {
            if ( atts.getLength() == 1 && atts.getQName(0).equals( "name" ) ) {
                // System.out.printf( "   attribute: %s = %s\n", atts.getQName(0), atts.getValue(0) );
                this.currentPipelineList = atts.getValue(0);
                if ( !this.pipelineList.containsKey( this.currentPipelineList ) ) {
                    // Create a new list of filters
                    this.filtersList = new ArrayList<String>();
                } else {
                    this.pipelineList.remove( this.currentPipelineList );
                }
            } else {
                System.out.printf( "Warning: cannot find name attribute for element '%s'", localName );
            }
            this.parsingState = XMLParseState.ACTION;
        } else if ( localName.equals( Constants.FILTER_ACTION ) ) {
            if ( atts.getLength() == 1 && atts.getQName(0).equals( "name" ) ) {
                // System.out.printf( "   attribute: %s = %s\n", atts.getQName(0), atts.getValue(0) );
                this.filtersList.add( atts.getValue(0) );
            } else {
                System.out.printf( "Warning: wrong number of attributes (%d) for element '%s'", atts.getLength(), localName );
            }
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     */
    
    public void endElement( String namespaceURI, String localName, String qName ) throws SAXException
    {
        // System.out.printf( "End element: %s\n", localName );
        if ( localName.equals( Constants.FILTER_ROOT ) && this.parsingState == XMLParseState.PIPELINE ) {
            this.parsingState = XMLParseState.ROOT;
        } else if ( localName.equals( Constants.FILTER_PIPELINE ) ) {
            this.parsingState = XMLParseState.PIPELINE;
            this.pipelineList.put( this.currentPipelineList, this.filtersList );
            // System.out.printf( "this.currentPipelineList.size: %d\n", this.pipelineList.size() );
        } else if ( localName.equals( Constants.FILTER_ACTION ) ) {
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     */
    
    public void endDocument() throws SAXException {
    }
    
}
