package uk.org.hsfr.antweave;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import java.io.IOException;

import java.net.URL;

import java.lang.ClassLoader;

import javax.xml.parsers.DocumentBuilder;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.dom.DOMSource;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
// XRefFilter.java
//
// Cross references the code building the defines and used-in information. The
// work is done using an XSL filter, addXref.xsl.
//
// Author
// ------
// Name  : Hugh Field-Richards.
// Email : hsfr@hsfr.org.uk
//
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

class XRefFilter implements Runnable
{
    private BufferedWriter bufferedOutputStream = null;
    private BufferedReader bufferedInputStream = null;
    private Filters filters = null;
    
    private String className = "";
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set up the XRefFilter pipeline component class.
     *
     * @param inputStream The input from the previous stage of the pipeline
     * @param outputStream The output stream to the next stage of the pipeline
     * @param filters The list of filters that we have to invoke
     * @exception Exception if problem creating the producer
     */
    
    public XRefFilter( PipedReader inputStream, PipedWriter outputStream, Filters filters ) throws Exception
    {
        try {
            this.bufferedInputStream = new BufferedReader( inputStream );
            this.bufferedOutputStream = new BufferedWriter( outputStream );
            this.filters = filters;
            this.className = this.getClass().getSimpleName();
        } catch( Exception e ) {
            System.out.printf( "Error creating %s filter: %s\n", this.className, e.toString() );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Runs the XRefFilter thread.
     */
    
    public void run()
    {
        try {
            // System.out.printf( "%s: running\n", this.className );
            // The transform file is stored in the filters folder
            String transformFileName = String.format( "%s%s%s", Filters.getDefaultFiltersLocation(), Constants.PATH_SEPARATOR, Constants.XREF_TRANSFORM_FILE_NAME );
            // System.out.printf( "%s: transformFileName: %s\n", this.className, transformFileName );
            FileInputStream fis = new FileInputStream( transformFileName );
            InputStream XSTLInputStream = fis;
            
            // Create a document factory to hold the piped data
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware( true );
            factory.setValidating( false );
            DocumentBuilder builder = factory.newDocumentBuilder();
            
            // Associate the input stream with the factory to build a document to transform
            Document document = builder.parse( new InputSource( this.bufferedInputStream ) );
            // Constants.prettyPrint( document );
            
            // Now set up the transformer factory and associate it with the XSL stream
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            StreamSource XSLTFile = new StreamSource( XSTLInputStream );
            Transformer transformer = transformerFactory.newTransformer( XSLTFile );
            
            // Form up a DOM of the piped data. Aassuming it is valid - machine generated so it should be :-)
            DOMSource source = new DOMSource( document );
            // StreamResult result = new StreamResult( System.out );
            StreamResult result = new StreamResult( this.bufferedOutputStream );
            transformer.transform( source, result );
            
            this.bufferedOutputStream.close();
            // System.out.printf( "%s: finished\n", this.className );
        } catch ( TransformerConfigurationException tce ) {
            // Error generated by the parser
            System.out.println("\n** Transformer Factory error");
            System.out.println("   " + tce.getMessage());
            // Use the contained exception, if any
            Throwable x = tce;
            if (tce.getException() != null) {
                x = tce.getException();
            }
            x.printStackTrace();
        } catch ( TransformerException te ) {
            // Error generated by the parser
            System.out.println("\n** Transformation error");
            System.out.println("   " + te.getMessage());
            // Use the contained exception, if any
            Throwable x = te;
            if (te.getException() != null) {
                x = te.getException();
            }
            x.printStackTrace();
        } catch (SAXException sxe) {
            // Error generated by this application
            // (or a parser-initialization error)
            Exception x = sxe;
            if (sxe.getException() != null) {
                x = sxe.getException();
            }
            x.printStackTrace();
        } catch (ParserConfigurationException pce) {
            // Parser with specified options can't be built
            pce.printStackTrace();
        } catch (IOException ioe) {
            // I/O error
            ioe.printStackTrace();
        } catch( Exception e ) {
            System.out.printf( "%s error: \n", this.className, e.toString() );
        }
    }
    
}



