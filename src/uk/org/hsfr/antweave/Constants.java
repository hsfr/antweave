package uk.org.hsfr.antweave;

import java.io.Writer;
import java.io.StringWriter;

import org.w3c.dom.Document;

import javax.xml.transform.Transformer;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
// Constants.java
//
// Author
// ------
// Name  : Hugh Field-Richards.
// Email : hsfr@hsfr.org.uk
//
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

class Constants
{
    public static final String ROOT_CHUNK = "*";
    
    public static final String INCLUDE_NOWEB_FILE = "^\\\\includeNoweb\\{(.+)\\}";
    
    public static final String PRODUCED_LINE = "^\\[\\[\\[(.+)\\]\\]\\]\\[\\[\\[(.+)\\]\\]\\](.*)$";
    
    public static final String CODE_CHUNK_NAME = "<<(.*)>>=";
    public static final String CODE_END = "^@\\s*$";
    public static final String CODE_END_DEF = "^@\\s+(%def.*)$";
    public static final String INCLUDE_CHUNK = "^(.*)<<(.*)>>(.*)$";
    
    public static final String CODE_LINE = "<aw:code([^>]+)><!\\[CDATA\\[(.*)\\]\\]></aw:code>";
    public static final String INCLUDE_LINE = "<aw:code([^>]+)><!\\[CDATA\\[(.*)\\]\\]><aw:include([^>]+)><!\\[CDATA\\[(.*)\\]\\]></aw:code>";
    
    public static final String NOWEB_NAMESPACE = "http://www.hsfr.org.uk/Schema/AntWeave";
    public static final String XREF_TRANSFORM_FILE_NAME = "addXRef.xsl";
    
    // Default for C, Swift, Java etc
    public static final String COMMENT_START = "//";
    public static final String COMMENT_END = "";
    
    // Program filter pipeline file
    public static final String DEFAULT_FILTER_FILE_NAME = "filters.xml";
    public static final String DEFAULT_FILE_TYPES_NAME = "types.xml";

    public static final String LOCAL_JAVA_EXTENSIONS_FOLDERS = "/Library/Java/Extensions";

    // XML constants for pipeline filter declaration
    public static final String FILTER_ROOT = "noweb";
    public static final String FILTER_PIPELINE = "pipeline";
    public static final String FILTER_ACTION = "filter";
    
    public static final String FILE_TYPE_ROOT = "noweb";
    public static final String FILE_TYPE_EXT = "type";
    public static final String EXT_ATTRIBUTE = "ext";
    public static final String START_COMMENT_ATTRIBUTE = "start";
    public static final String END_COMMENT_ATTRIBUTE = "end";
    
    // This is a bodge to solve a nasty default buffer size problem with Mac OS X
    public static final int OUTPUT_BUFFER_SIZE = 2^16;
    
    public static final String FAIL_MSG = "Processing failed.";
    
    public static final String PATH_SEPARATOR = "/";
    public static final String EXTENSION_SEPARATOR = ".";
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Convenience debug function
     */
    
    public static final void prettyPrint(Document xml) throws Exception
    {
        Transformer tf = TransformerFactory.newInstance().newTransformer();
        tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        tf.setOutputProperty(OutputKeys.INDENT, "yes");
        Writer out = new StringWriter();
        tf.transform(new DOMSource(xml), new StreamResult(out));
        System.out.println(out.toString());
    }
    
}




