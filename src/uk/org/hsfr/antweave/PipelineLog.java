package uk.org.hsfr.antweave;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PipedReader;
import java.io.FileWriter;

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
// PipelineLog.java
//
// Utility class.
//
// Author
// ------
// Name  : Hugh Field-Richards.
// Email : hsfr@hsfr.org.uk
//
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

class PipelineLog implements Runnable
{
    private BufferedReader bufferedInputStream = null;
    private BufferedWriter bufferedOutputStream = null;
    private String destFileName = null;
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    
    public PipelineLog( PipedReader inputStream, String destDir, String baseName ) throws Exception
    {
        try {
            this.bufferedInputStream = new BufferedReader( inputStream );
            this.destFileName = String.format( "%s%s%s.xml", destDir, Constants.PATH_SEPARATOR, baseName );
            this.bufferedOutputStream = new BufferedWriter( new FileWriter( this.destFileName ) );
        } catch( Exception e ) {
            System.out.println( "Error creating PipelineLog filter: " + e );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    
    public void run()
    {
        String inputLine = "";
        int lineNumber = 1;
        
        try {
            while ( ( inputLine = this.bufferedInputStream.readLine() ) != null ) {
                this.bufferedOutputStream.write( String.format( "%s\n", inputLine ) );
                lineNumber++;
            } // while not finished
            this.bufferedInputStream.close();
            this.bufferedOutputStream.close();
        } catch( Exception e ) {
            System.out.println( "PipelineLog: Error: " + e); 
        } 
    } 
}
