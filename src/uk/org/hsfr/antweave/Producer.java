package uk.org.hsfr.antweave;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PipedWriter;

import java.util.Random;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
// Producer.java
//
// This class reads a noweb file and injects it into the noweb pipeline for
// processing. It expands any include commands (includeNoweb) and tags each
// line with the file and line number within the file.
//
// Author
// ------
// Name  : Hugh Field-Richards.
// Email : hsfr@hsfr.org.uk
//
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

class Producer implements Runnable
{
    private String sourceFullFileName = "";
    private String sourcePathName = "";
    private BufferedWriter bufferedOutputStream = null;
    private String className = "";
    private Random rand = new Random();
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set up the producer with file names etc.
     *
     * It creates the thread's input and output streams.
     *
     * @param fileName The source noweb file
     * @param outputStream The output stream to the next stage of the pipeline
     * @exception Exception if problem creating the producer
     */
    
    public Producer( String fileName, PipedWriter outputStream ) throws Exception
    {
        try {
            this.sourceFullFileName = fileName;
            this.bufferedOutputStream = new BufferedWriter( outputStream );
            File f = new File( this.sourceFullFileName );
            this.sourcePathName = f.getParent();
            this.className = this.getClass().getSimpleName();
        } catch( Exception e ) {
            System.out.println( "Error creating Producer: " + e );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Runs the Producer thread.
     *
     * Method calls the readFile method with the base source name.
     */
    
    public void run()
    {
        try {
            System.out.printf( "%s\n", this.sourceFullFileName );
            this.readFile( this.sourceFullFileName );
            this.bufferedOutputStream.close();
        } catch( IOException e ) {
            System.out.println( "Producer run error: " + e );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Reads the file and expands any includes.
     *
     * This recursive method reads from the file and inserts the next line into the
     * pipeline. It continues until the file end is reached.
     * Lines are output into the pipeline in the format
     *
     *      [[[file name]]][[[line number]]]source line
     *
     * @param fileName The file name of the file to be read.
     * @exception IOException if problem reading the file
     */
    
    public void readFile( String fileName ) throws IOException
    {
        BufferedReader bufferedInputStream = new BufferedReader( new FileReader( fileName ) );
        File f = new File( fileName );
        
        String inputLine = "";
        int lineNumber = 1;
        PatternInfo patternResult;
        
        while ( bufferedInputStream.ready() ) {
            inputLine = bufferedInputStream.readLine();
            patternResult = isIncludeFile( inputLine );
            if ( patternResult.found ) {
                String fullFileName = String.format( "%s%s%s", this.sourcePathName, Constants.PATH_SEPARATOR, patternResult.pattern[1] );
                System.out.printf( "Including file %s\n", fullFileName );
                this.readFile( fullFileName );
            } else {
                this.bufferedOutputStream.write( String.format( "[[[%s]]][[[%d]]]%s\n", f.getName(), lineNumber, inputLine ) );
            }
            lineNumber++;
        }
        bufferedInputStream.close();
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Does the line match Constants.INCLUDE_NOWEB_FILE string?
     *
     * Searches line for regular expression string.
     *
     * @param line The line to search.
     * @return PatternInfo (true, found pattern) if found, otherwise (false).
     */
    
    private PatternInfo isIncludeFile( String line )
    {
        Matcher m = Pattern.compile( Constants.INCLUDE_NOWEB_FILE ).matcher( line );
        if ( m.find() ) {
            String foundPattern = m.group( 1 );
            return new PatternInfo( true, foundPattern ) ;
        }
        return new PatternInfo( false ) ;
    }
    
}
