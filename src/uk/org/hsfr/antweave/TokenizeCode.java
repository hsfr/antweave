package uk.org.hsfr.antweave;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PipedReader;
import java.io.PipedWriter;

import java.lang.Character;

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
// TokenizeCode.java
//
// Goes through the code sections marking off the tokens within the code. This helps
// the weave indexer later on. It is ignored by the tangle filter (removes the markup).
//
// Author
// ------
// Name  : Hugh Field-Richards.
// Email : hsfr@hsfr.org.uk
//
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * TokenizeCode takes each code statement and splits them up into a set of separated
 * tokens that makes the job of the cross referencing filter more easy. It bases the
 * separators on the Java set (a reasonable assumption) and tokenises the line
 * including reserved words and operators. Again this is a reasonable assumption
 * because the user is unlikely to define these.
 *
 * @author Hugh Field-Richards
 */

class TokenizeCode implements Runnable
{
    private BufferedWriter bufferedOutputStream = null;
    private BufferedReader bufferedInputStream = null;
    private Thread sourceThread = null;
    private String className = "";
    
    public enum ParseState {
        LINE_START,
        IN_SEPARATOR,
        IN_TOKEN
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set up the input and output streams.
     *
     * @param inputStream The input from the previous stage of the pipeline
     * @param outputStream The output stream to the next stage of the pipeline
     * @exception Exception if problem creating the producer
     */
    
    public TokenizeCode( PipedReader inputStream, PipedWriter outputStream ) throws Exception
    {
        try {
            this.bufferedInputStream = new BufferedReader( inputStream );
            this.bufferedOutputStream = new BufferedWriter( outputStream );
            this.className = this.getClass().getSimpleName();
        } catch( Exception e ) {
            System.out.println( "Error creating TokenizeCode filter: " + e );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Runs the TokenizeCode thread.
     */
    
    public void run()
    {
        // System.out.printf( "%s: running\n", this.className );
        try {
            String inputLine = "";
            Matcher m;
            String group_1 = "";
            String group_2 = "";
            String group_3 = "";
            String group_4 = "";
            
            PatternInfo patternResult;
            boolean found = false;
            
            while ( ( inputLine = this.bufferedInputStream.readLine() ) != null ) {
                group_1 = "";
                group_2 = "";
                group_3 = "";
                group_4 = "";
                found = false;
                // System.out.printf( "inputLine: \"%s\"\n", inputLine );
                
                patternResult = isIncludeLine( inputLine );
                if ( !found && patternResult.found ) {
                    group_1 = patternResult.pattern[1];
                    group_2 = patternResult.pattern[2];
                    group_3 = patternResult.pattern[3];
                    group_4 = patternResult.pattern[4];
                    // System.out.printf( "includeLine:'%s'\n", inputLine );
                    this.bufferedOutputStream.write( String.format( "%s\n", this.tokenizeIncludeLine( group_1, group_2, group_3, group_4 ) ) );
                    found = true;
                }
                
                patternResult = isCodeLine( inputLine );
                if ( !found && patternResult.found ) {
                    group_1 = patternResult.pattern[1];
                    group_2 = patternResult.pattern[2];
                    //System.out.printf( "codeLine:'%s'\n", inputLine );
                    this.bufferedOutputStream.write( String.format( "%s\n", this.tokenizeCodeLine( group_1, group_2 ) ) );
                    found = true;
                }
                
                if ( !found ) {
                    this.bufferedOutputStream.write( String.format( "%s\n", inputLine ) );
                }
                
            } // while not finished
            this.bufferedOutputStream.flush();
            this.bufferedOutputStream.close();
            this.bufferedInputStream.close();
            // System.out.printf( "Code tokeniser finished\n" );
        } catch( Exception e ) {
            System.out.println( "TagSource: Error: " + e );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * This method scans a line of code and marks off any tokens it finds. These
     * are defined as any that are not separators on the Java set.
     */
    private String tokenizeCodeLine( String codeAttributes, String codeLine )
    {
        int charIndex = 0;
        ParseState parsingState = ParseState.LINE_START;
        ParseState oldParsingState = parsingState;
        
        String tokenizedLine = String.format( "<aw:code %s>", codeAttributes );
        tokenizedLine = tokenizedLine + this.tokenizeCode( codeLine );
        return tokenizedLine + "</aw:code>";
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     */
    private String tokenizeIncludeLine( String codeAttributes, String includePrefix, String includeAttributes, String includeSuffix )
    {
        int charIndex = 0;
        ParseState parsingState = ParseState.LINE_START;
        ParseState oldParsingState = parsingState;
        
        // System.out.printf( "includeSuffix '%s'\n", includeSuffix );
        String tokenizedLine = String.format( "<aw:code %s>", codeAttributes );
        tokenizedLine = tokenizedLine + this.tokenizeCode( includePrefix );
        tokenizedLine = tokenizedLine + String.format( "<aw:include %s>", includeAttributes );
        tokenizedLine = tokenizedLine + this.tokenizeCode( includeSuffix );
        // System.out.printf( "tokenizedLine '%s'\n", tokenizedLine );
        return tokenizedLine + "</aw:code>";
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Takenizes a line (or string) of code into component parts.
     *
     * @param codeLine the code string to parse into tokens
     * @return String the tokenized code string
     */
    private String tokenizeCode( String codeLine )
    {
        int charIndex = 0;
        char[] chars = codeLine.toCharArray();
        ParseState parsingState = ParseState.LINE_START;
        ParseState oldParsingState = parsingState;
        
        String identString = "";
        String separatorString = "";
        String tokenizedString = "";
        //System.out.printf( "codeAttributes:'%s' codeLine:'%s' size: %d\n", codeAttributes, codeLine, chars.length );
        
        while ( charIndex < chars.length ) {
            char ch = chars[charIndex];
            //System.out.printf( "[parsingState: %s, charIndex:%d, ch:'%c']\n", parsingState, charIndex, ch );
            oldParsingState = parsingState;
            
            if ( Character.isJavaIdentifierPart( ch ) ) {
                //System.out.printf( "[isJavaIdentifierPart]" );
                if ( parsingState == ParseState.LINE_START ) {
                    parsingState = ParseState.IN_TOKEN;
                    identString = "<aw:token><![CDATA[" + Character.toString( ch );
                    //System.out.printf( "   start of variable after line start\n" );
                } else if ( parsingState == ParseState.IN_SEPARATOR ) {
                    parsingState = ParseState.IN_TOKEN;
                    identString = "]]></aw:separator><aw:token><![CDATA[" + Character.toString( ch );
                    tokenizedString += separatorString;
                    //System.out.printf( "   found separator '%s'\n", separatorString );
                } else if ( parsingState == ParseState.IN_TOKEN ) {
                    identString += ch;
                    //System.out.printf( "   in variable\n" );
                }
            } else {
                //System.out.printf( "[isNotJavaIdentifierPart]\n" );
                if ( parsingState == ParseState.LINE_START ) {
                    parsingState = ParseState.IN_SEPARATOR;
                    separatorString = "<aw:separator><![CDATA[" + Character.toString( ch );
                    //System.out.printf( "   start of separator after line start\n" );
                } else if ( parsingState == ParseState.IN_TOKEN ) {
                    parsingState = ParseState.IN_SEPARATOR;
                    separatorString = "]]></aw:token><aw:separator><![CDATA[" + Character.toString( ch );
                    tokenizedString += identString;
                    //System.out.printf( "   found variable '%s'\n", identString );
                } else if ( parsingState == ParseState.IN_SEPARATOR ) {
                    separatorString += ch;
                    //System.out.printf( "   in separator\n" );
                }
            }
            charIndex++;
        }
        
        if ( parsingState == ParseState.IN_SEPARATOR ) {
            tokenizedString += separatorString + "]]></aw:separator>";
            //System.out.printf( "   found separator '%s'\n", separatorString );
        } else if ( parsingState == ParseState.IN_TOKEN ) {
            tokenizedString += identString + "]]></aw:token>";
            //System.out.printf( "   found variable '%s'\n", identString );
        }
        //System.out.printf( "tokenizedString '%s'\n", tokenizedString );
        return tokenizedString;
    }
    
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    
    private PatternInfo isCodeLine( String line )
    {
        Matcher m = Pattern.compile( Constants.CODE_LINE ).matcher( line );
        if ( m.find() ) {
            return new PatternInfo( true, m.group(1), m.group(2) );
        }
        return new PatternInfo( false ) ;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    
    private PatternInfo isIncludeLine( String line )
    {
        Matcher m = Pattern.compile( Constants.INCLUDE_LINE ).matcher( line );
        if ( m.find() ) {
            return new PatternInfo( true, m.group(1), m.group(2), m.group(3), m.group(4) );
        }
        return new PatternInfo( false ) ;
    }
    
}



