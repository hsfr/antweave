package uk.org.hsfr.antweave;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Hashtable;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PipedReader;
import java.io.PipedWriter;

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
// TagSource.java
//
// Translates a noweb file into an XML representation suitable for
// further processing in the pipeline.
//
// Author
// ------
// Name  : Hugh Field-Richards.
// Email : hsfr@hsfr.org.uk
//
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

class TagSource implements Runnable
{
    private BufferedWriter bufferedOutputStream = null;
    private BufferedReader bufferedInputStream = null;
    private String baseName = "";
    
    private Thread sourceThread = null;
    private String className = "";
    
    private String chunkHash = "";
    
    private static final String HASH_SEPARATOR = "-";
    
    Hashtable<String, Integer> chunkList = new Hashtable<String, Integer>();
    
    public enum ParseState {
        DOCUMENTATION_START,
        DOCUMENTATION_TEXT,
        DOCUMENTATION_NEWLINE,
        CHUNK_INCLUDE,
        CODE_START,
        CODE_TEXT,
        CODE_END
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set up the source tagger with the input and streams.
     *
     * It creates the thread's input and output streams.
     *
     * @param inputStream The input from the previous stage of the pipeline
     * @param outputStream The output stream to the next stage of the pipeline
     * @param baseName The file name
     * @exception Exception if problem creating the producer
     */
    
    public TagSource( PipedReader inputStream, PipedWriter outputStream, String baseName ) throws Exception
    {
        try {
            this.bufferedInputStream = new BufferedReader( inputStream );
            this.bufferedOutputStream = new BufferedWriter( outputStream );
            this.baseName = baseName;
            this.className = this.getClass().getSimpleName();
        } catch( Exception e ) {
            System.out.println( "Error creating TagSource filter: " + e );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Runs the TagSource thread.
     */
    
    public void run()
    {
        // System.out.printf( "%s: running\n", this.className );
        try {
            String inputLine = "";
            int chunkNumber = 1;
            int chunkToInclude = 0;
            String lineNumber = "";
            String currentFile = "";
            
            // The number to keep track of code chunks spread over several same name chunks
            int seriesChunkIndex = 1;
            String hashPrefix = "NW" + Integer.toHexString( baseName.hashCode() ) + HASH_SEPARATOR;
            
            ParseState parsingState = ParseState.DOCUMENTATION_START;
            ParseState oldParsingState = parsingState;
            int numberOfMatches = 0;
            
            Matcher m;
            String group_1 = "";
            String group_2 = "";
            String group_3 = "";
            
            PatternInfo patternResult;
            boolean found = false;
            
            this.bufferedOutputStream.write( String.format( "<?xml version='1.0' encoding='UTF-8'?>\n" ) );
            this.bufferedOutputStream.write( String.format( "<aw:file xmlns:aw='%s'>\n", Constants.NOWEB_NAMESPACE ) );
            
            while ( ( inputLine = this.bufferedInputStream.readLine() ) != null ) {
                group_1 = "";
                group_2 = "";
                group_3 = "";
                oldParsingState = parsingState;
                found = false;
                
                //System.out.print( String.format( "%s\n",inputLine ) );
                patternResult = splitInputLine( inputLine );
                currentFile = patternResult.pattern[1];
                lineNumber = patternResult.pattern[2];
                inputLine = patternResult.pattern[3];
                // System.out.print( String.format( "%s:%s:%s\n",currentFile, lineNumber, inputLine ) );
                
                patternResult = isCodeStartChunk( inputLine );
                if ( !found && patternResult.found ) {
                    parsingState = ParseState.CODE_START;
                    group_1 = patternResult.pattern[1];
                    found = true;
                }
                patternResult = isCodeEndChunk( inputLine );
                if ( !found && patternResult.found ) {
                    parsingState = ParseState.CODE_END;
                    group_1 = patternResult.pattern[1];
                    found = true;
                }
                patternResult = isIncludeChunk( inputLine );
                if ( !found && patternResult.found ) {
                    parsingState = ParseState.CHUNK_INCLUDE;
                    group_1 = patternResult.pattern[1];
                    group_2 = patternResult.pattern[2];
                    group_3 = patternResult.pattern[3];
                    // System.out.print( String.format( "[%s -> %s][%s][%s][%s]\n", oldParsingState, parsingState, group_1, group_2, group_3 ) );
                    found = true;
                }
                // System.out.print( String.format( "[%s -> %s][%s] %s\n", oldParsingState, parsingState, group_1, inputLine ) );
                
                switch ( parsingState ) {
                        
                    case DOCUMENTATION_START :
                        this.bufferedOutputStream.write( String.format( "<aw:chunk type='%s' file='%s' line='%s'>\n", "documentation", currentFile, lineNumber ) );
                        this.bufferedOutputStream.write( String.format( "<aw:text line='%s'><![CDATA[%s]]></aw:text>\n", lineNumber, inputLine ) );
                        parsingState = ParseState.DOCUMENTATION_TEXT;
                        break;
                        
                    case DOCUMENTATION_TEXT :
                        this.bufferedOutputStream.write( String.format( "<aw:text line='%s'><![CDATA[%s]]></aw:text>\n", lineNumber, inputLine ) );
                        break;
                        
                    case CHUNK_INCLUDE :
                        this.bufferedOutputStream.write( String.format("<aw:code file='%s' line='%s'><![CDATA[%s]]>", currentFile, lineNumber, group_1 ) );
                        this.bufferedOutputStream.write( String.format( "<aw:include file='%s' line='%s' ref='%s' hash='%s' />",
                                                                       currentFile, lineNumber, group_2, this.chunkHash + HASH_SEPARATOR + seriesChunkIndex ) );
                        this.bufferedOutputStream.write( String.format("<![CDATA[%s]]></aw:code>\n", group_3 ) );
                        parsingState = ParseState.CODE_TEXT;
                        break;
                        
                    case CODE_START :
                        if ( oldParsingState == ParseState.DOCUMENTATION_TEXT ) {
                            this.bufferedOutputStream.write( String.format( "</aw:chunk>\n" ) );
                            // this.bufferedOutputStream.write( String.format( "<!-- Start of code chunk name='%s' -->\n", group_1 ) );
                        }
                        this.chunkHash = hashPrefix + Integer.toHexString( group_1.hashCode() );
                        if ( chunkList.containsKey( this.chunkHash ) ) {
                            // We have an entry on the hash table already. Get the counter.
                            seriesChunkIndex = chunkList.get( this.chunkHash ).intValue();
                            seriesChunkIndex++;
                            chunkList.put( this.chunkHash, Integer.valueOf( seriesChunkIndex ) );
                        } else {
                            // Put in new entry starting at 1
                            seriesChunkIndex = 1;
                            chunkList.put( this.chunkHash, Integer.valueOf( seriesChunkIndex ) );
                        }
                        
                        this.bufferedOutputStream.write( String.format( "<aw:chunk type='%s' file='%s' line='%s' id='%d' hash='%s' name='%s'>\n",
                                                                       "code", currentFile, lineNumber, chunkNumber, this.chunkHash + HASH_SEPARATOR + seriesChunkIndex, group_1 ) );
                        parsingState = ParseState.CODE_TEXT;
                        break;
                        
                    case CODE_END :
                        if ( !group_1.equals( "" ) ) {
                            this.bufferedOutputStream.write( String.format("</aw:chunk>\n" ) );
                            this.bufferedOutputStream.write( String.format("<aw:definitions chunk='%s'>\n", this.chunkHash + HASH_SEPARATOR + seriesChunkIndex ) );
                            String[] splitDefs = group_1.split( "\\s+" );
                            // start at 1 to remove %def
                            for ( int x=1; x<splitDefs.length; x++ ) {
                                this.bufferedOutputStream.write( String.format("<aw:definition>%s</aw:definition>\n", splitDefs[x] ) );
                            }
                            this.bufferedOutputStream.write( String.format("</aw:definitions>\n" ) );
                        } else {
                            this.bufferedOutputStream.write( String.format("</aw:chunk>\n" ) );
                        }
                        //this.bufferedOutputStream.write( String.format("<!-- End of chunk: %s [%s] -->\n", chunkNumber, this.chunkHash + HASH_SEPARATOR + seriesChunkIndex ) );
                        parsingState = ParseState.DOCUMENTATION_START;
                        chunkNumber++;
                        break;
                        
                    case CODE_TEXT :
                        this.bufferedOutputStream.write( String.format("<aw:code file='%s' line='%s'><![CDATA[%s]]></aw:code>\n", currentFile, lineNumber, inputLine ) );
                        break;
                        
                    default:
                        this.bufferedOutputStream.write( String.format("<aw:line file='%s' line='%s'>%s</aw:line>\n", currentFile, lineNumber, inputLine ) );
                        break;
                }
                
            } // while not finished
            if ( parsingState == ParseState.DOCUMENTATION_TEXT || parsingState == ParseState.CODE_TEXT ) {
                this.bufferedOutputStream.write( String.format("</aw:chunk>\n" ) );
            }
            this.bufferedOutputStream.write( String.format( "</aw:file>\n" ) );
            this.bufferedOutputStream.flush();
            this.bufferedOutputStream.close();
            // System.out.printf( "%s: finished\n", this.className );
        } catch( Exception e ) {
            System.out.println( "TagSource: Error: " + e );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Splits the inputted line.
     *
     * @param line The line to split.
     * @return An instance of PatternInfo which has the various parts of the line. True is returned if pattern is matched.
     */
    
    private PatternInfo splitInputLine( String line )
    {
        Matcher m = Pattern.compile( Constants.PRODUCED_LINE ).matcher( line );
        if ( m.find() ) {
            return new PatternInfo( true, m.group(1), m.group(2), m.group(3) ) ;
        }
        return new PatternInfo( false ) ;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Splits the include line.
     *
     * @param line The line to split
     * @return An instance of PatternInfo which has the various parts of the line. True is returned if pattern is matched.
     */
    
    private PatternInfo isIncludeChunk( String line )
    {
        Matcher m = Pattern.compile( Constants.INCLUDE_CHUNK ).matcher( line );
        if ( m.find() ) {
            return new PatternInfo( true, m.group(1), m.group(2), m.group(3) ) ;
        }
        return new PatternInfo( false ) ;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Is this the start of a code chunk.
     *
     * @param line The line to split
     * @return An instance of PatternInfo which has the various parts of the line. True is returned if pattern is matched.
     */
    
    private PatternInfo isCodeStartChunk( String line )
    {
        Matcher m = Pattern.compile( Constants.CODE_CHUNK_NAME ).matcher( line );
        if ( m.find() ) {
            return new PatternInfo( true, m.group(1) ) ;
        }
        return new PatternInfo( false ) ;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Is this the end of a code chunk.
     *
     * @param line The line to split
     * @return An instance of PatternInfo which has the various parts of the line. True is returned if pattern is matched.
     */
    
    private PatternInfo isCodeEndChunk( String line )
    {
        Matcher m = Pattern.compile( Constants.CODE_END ).matcher( line );
        if ( m.find() ) {
            String foundPattern = "";
            return new PatternInfo( true, foundPattern ) ;
        }
        m = Pattern.compile( Constants.CODE_END_DEF ).matcher( line );
        if ( m.find() ) {
            return new PatternInfo( true, m.group(1) ) ;
        }
        return new PatternInfo( false ) ;
    }
    
}



