package uk.org.hsfr.antweave;

import java.io.File;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.ListIterator;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.apache.tools.ant.Task;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;

import org.apache.tools.ant.util.FileUtils;
import org.apache.tools.ant.util.GlobPatternMapper;
import org.apache.tools.ant.util.SourceFileScanner;

import org.apache.tools.ant.types.Path;

import org.apache.tools.ant.taskdefs.MatchingTask;

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
 * The AntWeave Ant task is an implementation of the NoWeb literate-programming tool. NoWeb was
 * designed by Norman Ramsey. Because AntWeave uses
 * Java (and the XML/XSL environment) it is universal to systems running Java. There are some
 * differences and assumptions that this
 * implementation uses compared to the original NoWeb. 
 * <p>
 * This software is covered by several copyrights.
 * <p>
 * NOWEB
 * <p>
 * Noweb is copyright 1989-2000 by Norman Ramsey.  All rights reserved.
 * <p>
 * Noweb is protected by copyright.  It is not public-domain
 * software or shareware, and it is not protected by a "copyleft"
 * agreement like the one used by the Free Software Foundation.
 * <p>
 * Noweb is available free for any use in any field of endeavor.  You may
 * redistribute noweb in whole or in part provided you acknowledge its
 * source and include this COPYRIGHT file.  You may modify noweb and
 * create derived works, provided you retain this copyright notice, but
 * the result may not be called noweb without Norman Ramsey's written consent.
 * <p>
 * You may sell noweb if you wish.  For example, you may sell a CD-ROM
 * including noweb.
 * <p>
 * You may sell a derived work, provided that all source code for your
 * derived work is available, at no additional charge, to anyone who buys
 * your derived work in any form.  You must give permisson for said
 * source code to be used and modified under the terms of this license.
 * You must state clearly that your work uses or is based on noweb and
 * that noweb is available free of change.  You must also request that
 * bug reports on your work be reported to you.
 * <p>
 * APACHE
 * <p>
 * Ant is licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with this
 * work for additional information regarding copyright ownership. The ASF
 * licenses this file to You under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * <p>
 * HUGH FIELD-RICHARDS
 * <p>
 * The code which makes up this Ant task is free software: you can redistribute
 * it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details, http://www.gnu.org/licenses/
 *
 * @author Hugh Field-Richards
 * @version 1.0.0
 */

public class AntWeave extends MatchingTask {
    
    /** The directory to scan for noweb files */
    private Path srcDir = null;
    
    /** The destination directory */
    private File destDir = null;
    
    /** The list of files to be processed */
    private File[] processList = new File[0];
    private Map<String, Long> packageInfos = new HashMap<String, Long>();
    
    /** The operation to perform. This is picked up in the filters.xml file and there should be
     an entry in that file, for example <pipeline name="tangle">. The action defaults to "tangle". */
    private String requiredAction = "tangle";
    
    /** The destination file if specified */
    private File destFile = null;
    
    /** The extension to use if specified */
    private String destFileExt = "";
    
    /** The user filters file that overrides the default one */
    private File filtersFile = null;
    
    /** Comment characters for commenting the output */
    private String commentStartString = null;
    private String commentEndString = null;
    
    /** The chunk that is the root of all the output (only relevant with tangle) */
    private String rootChunk = "";
    
    protected boolean listFiles = true;
    
    /** How the weave output for LaTeX is wrapped */
    private boolean delay = false;
    
    private boolean stats = false;
    
    /** A Task variable */
    private boolean failOnError = true;
    
    protected FileUtils fileUtils;
    private long granularity = 0;
    protected int verbosity = Project.MSG_VERBOSE;
    private boolean taskSuccess = true; // assume the best
    private String errorProperty;
    
    private static final FileUtils FILE_UTILS = FileUtils.getFileUtils();
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Parameter/Attrib Settings -*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set a source directory to scan for noweb files.
     *
     * This is the only attribute that is required.
     *
     * @param srcDir the directory to scan.
     */
    
    public void setSrcdir( Path srcDir )
    {
        if ( this.srcDir == null ) {
            this.srcDir = srcDir;
        } else {
            this.srcDir.append( srcDir );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Adds a path for source compilation.
     *
     * @return a nested src element.
     */
    
    public Path createSrc() {
        if ( this.srcDir == null ) {
            // Create an empty Path
            this.srcDir = new Path( getProject() );
        }
        return this.srcDir.createPath();
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Recreate src.
     *
     * @return a nested src element.
     */
     
    protected Path recreateSrc() {
        this.srcDir = null;
        return createSrc();
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set the destination directory.
     *
     * @param destDir the destination directory.
     */
    
    public void setTodir( File destDir ) {
        this.destDir = destDir;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set the destination file.
     *
     * @param destFile the destination file.
     */
    
    public void setTofile( File destFile ) {
        this.destFile = destFile;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set the destination file extension.
     *
     * Defaults to "tex" for weave and "c" for tangle
     *
     * @param ext the extension.
     */
    
    public void setExt( String ext ) {
        this.destFileExt = ext;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set the delayed wrapper flag.
     *
     * @param delay "yes" etc if delay required.
     */
    
    public void setDelay( String delay ) {
        this.delay = ( delay.toLowerCase().equals( "yes" ) || delay.toLowerCase().equals( "true" ) );
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * If true, list the source files being handed off to the compiler.
     * @param list if true list the source files
     */
    public void setListfiles( boolean list ) {
        this.listFiles = list;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get the listfiles flag.
     * @return the listfiles flag
     */
    public boolean getListfiles() {
        return this.listFiles;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set the user filters file.
     *
     * @param filtersFile the user filters file.
     */
    
    public void setFilters( File filtersFile ) {
        this.filtersFile = filtersFile;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set the comment strings.
     *
     * @param commentString the comment string.
     */
    
    public void setCommentStart( String commentString ) {
        this.commentStartString = commentString;
    }
    
    public void setCommentEnd( String commentString ) {
        this.commentEndString = commentString;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set the root chunk (used in the tangle process). Note that this is used as
     * the destination file unless that file is explicitly given.
     *
     * @param rootChunk the root chunk name.
     */
    
    public void setRoot( String rootChunk ) {
        this.rootChunk = rootChunk;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    
    public void setAction( String action )
    {
        this.requiredAction = action;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set verbose mode. Used to force listing of all names of copied files.
     *
     * @param verbose whether to output the names of copied files. Default is false.
     */
    
    public void setVerbose( boolean verbose )
    {
        this.verbosity = verbose ? Project.MSG_INFO : Project.MSG_VERBOSE;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    
    public void setFailonerror( boolean inFlag )
    {
        this.failOnError = inFlag;
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * AntWeave task constructor.
     *
     * Set up the task file utilities and granularity of logging.
     */
    
    public AntWeave() {
        this.fileUtils = FileUtils.getFileUtils();
        this.granularity = this.fileUtils.getFileTimestampGranularity();
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get the FileUtils for this task.
     *
     * @return the fileutils object.
     */
    
    protected FileUtils getFileUtils() {
        return this.fileUtils;
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Display a message, preceded by the name of the current thread
     */
    
    static void threadMessage( String message )
    {
        String threadName = Thread.currentThread().getName();
        System.out.format( "%s: %s%n", threadName, message );
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Executes the task.
     *
     * @exception BuildException if an error occurs
     */
    
    public void execute() throws BuildException
    {
        checkForRequiredAttributes();
        resetSourceFileLists();
        
        // Scan for the files to be processed. First we find which directory to scan.
        // If source file attribute is specified (this.nowebFile) then just do that one.
        
        String[] list = this.srcDir.list();
        for ( int i = 0; i < list.length; i++ ) {
            File sourceDir = getProject().resolveFile( list[i] );
            if ( !sourceDir.exists() ) {
                throw new BuildException( String.format( "Source directory \"%s\" does not exist!", sourceDir.getPath() ), getLocation());
            }
            
            DirectoryScanner ds = this.getDirectoryScanner( sourceDir );
            String[] files = ds.getIncludedFiles();
            this.scanDir( sourceDir, this.destDir != null ? this.destDir : sourceDir, files, this.destFileExt );
            if ( this.processList.length > 0 ) {
                this.processFiles();
            } else {
                log( String.format( "No files to process" ) );
            }
        }
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Processes the file.
     *
     * @exception BuildException if an error occurs
     */
    
    public void processFiles() throws BuildException
    {
        Filters filters = null;
        CommentTypes commentTypes = null;

        if ( this.processList.length > 0 ) {
            // Set up to read the filters for this session. If the users file is not specified
            // then the default one is used.
            try {
                filters = new Filters( this.filtersFile );
                commentTypes = new CommentTypes( Constants.DEFAULT_FILE_TYPES_NAME );
                log( String.format( "%d filter(s) found", filters.numberOfPipelines() ), this.verbosity );
            } catch ( Exception ex ) {
                log( ex.getMessage(), verbosity );
                throw new BuildException( ex.getMessage() );
            }

            if ( listFiles ) {
                // System.out.printf( "listFiles: true\n" );

                for ( int i = 0; i < this.processList.length; i++ ) {
                    String filename = this.processList[i].getAbsolutePath();
                    
                    // Get basename of the source file (no path or extension)
                    String baseName = this.processList[i].getName();
                    if ( baseName.indexOf(".") > 0 ) {
                        baseName = baseName.substring( 0, baseName.lastIndexOf(".") );
                    }
                    log( String.format( "Source file: %s", this.processList[i].getName() ), this.verbosity );
                    // System.out.printf( "Source file: %s\n", this.processList[i].getName() );

                    // First sort out the destination directory
                    if ( this.destDir == null ) {
                        // Destination directory not explicitly given, so use the source directory
                        this.destDir = new File( this.processList[i].getParent() );
                    } else {
                        // Do nothing as it has been set
                    }
                    log( String.format( "Destination directory: %s", this.destDir.getParent() ), this.verbosity );
                    // System.out.printf( "Destination directory: %s\n", this.destDir.getParent() );

                    // Working out the file extension is a little more complex. We need to work out the extension
                    // in order to set the comment strings.
                    //
                    // There is a strict hierarchy on what determines the extension to be used.

                    if ( this.processList.length == 1 ) {
                        // One file set, is destination file given?
                        // System.out.printf( "this.destFile: %s\n", this.destFile );
                        if ( this.destFile != null ) {
                            // Yes - extract the extension, if present. This overrides the explicit extension
                            // in the attributes.
                            String fileExtension = this.getExtension( this.destFile.getName() );
                            // System.out.println( String.format( "fileExtension: %s", fileExtension ));
                            // If extension present use this
                            if ( !fileExtension.isEmpty() ) {
                                this.destFileExt = fileExtension;
                            }
                        } else {
                            // No destination file given - has the user given a root chunk (not using <<*>>)?
                            if ( !this.rootChunk.isEmpty() ) {
                                // Yes - is there an explicit extension given?
                                // System.out.println( String.format( "this.rootChunk: %s", this.rootChunk ) );
                                if ( !this.destFileExt.isEmpty() ) {
                                    // Yes so use that
                                    this.destFile = new File( String.format( "%s%s%s", this.rootChunk, Constants.EXTENSION_SEPARATOR, this.destFileExt ) );
                                } else {
                                    // No, so use the chunk by itself and hope the user has put in the start
                                    // and end comment strings.
                                    this.destFile = new File( this.rootChunk );
                                }
                                String fileExtension = this.getExtension( this.destFile.getName() );
                                // System.out.println( String.format( "fileExtension: %s", fileExtension ));
                                // If extension present use this
                                if ( !fileExtension.isEmpty() ) {
                                    this.destFileExt = fileExtension;
                                }
                                // System.out.println( String.format( "this.destFile: %s", this.destFile ) );
                            } else {
                                // No root chunk (or destination file) so use the source file. First check
                                // if the extension has been given
                                // System.out.printf( "this.destFileExt: %s\n", this.destFileExt );
                                if ( !this.destFileExt.isEmpty() ) {
                                    // Yes, an extension given so form up a destination file name from the source and extension
                                    this.destFile = new File( String.format( "%s%s%s", baseName, Constants.EXTENSION_SEPARATOR, this.destFileExt ) );
                                } else {
                                    // If it is a weave action assume that it is a TeX file
                                    // System.out.printf( "this.requiredAction: %s\n", this.requiredAction );
                                  if ( this.requiredAction.equals( "weave" ) ) {
                                        this.destFileExt = "tex";
                                        this.destFile = new File( String.format( "%s%s%s", baseName, Constants.EXTENSION_SEPARATOR, this.destFileExt ) );
                                      // System.out.println( String.format( "%s%s%s", baseName, Constants.EXTENSION_SEPARATOR, this.destFileExt ) );
                          } else {
                                        log( String.format( "Warning! Cannot form correct destination file+ext, using source file name without noweb extension.\n" ) );
                                        this.destFile = new File( String.format( "%s", baseName ) );
                                    }
                                }
                            }
                        } // ( this.destFile != null )
                    } else { // Several files specified
                        // More than one file being processed so use source file with suitable extension
                        if ( this.destFileExt == null || this.destFileExt.isEmpty() ) {
                            this.destFile = new File( String.format( "%s", baseName ) );
                        } else {
                            this.destFile = new File( String.format( "%s%s%s", baseName, Constants.EXTENSION_SEPARATOR, this.destFileExt ) );
                        }
                    }
                    
                    // System.out.println( String.format( "2: ================" ) );
                    // System.out.printf( "this.destFileExt: %s\n", this.destFileExt );
                    if ( this.destFileExt != null && !this.destFileExt.isEmpty() ) {
                        // System.out.println( String.format( "this.destFileExt: %s", this.destFileExt ));
                        commentStartString = commentTypes.getStartCommentString( this.destFileExt );
                        commentEndString = commentTypes.getEndCommentString( this.destFileExt );
                    } else {
                        if ( this.commentStartString == null || this.commentEndString == null ) {
                            System.out.println( String.format( "Warning! Comment strings not fully specified.\n" ) );
                        }
                        commentStartString = "";
                        commentEndString = "";
                    }
                    // System.out.println( String.format( "commentStartString: '%s'", commentStartString ) );
                    // System.out.println( String.format( "commentEndString: '%s'", commentEndString ) );

                    if ( commentStartString == null || commentEndString == null ) {
                        throw new BuildException( String.format( "Warning! Comment strings not set (null). Check correct extension in types.xml file" ) );
                    }
                    log( String.format( "this.requiredAction: '%s'", this.requiredAction ), this.verbosity );
                    log( String.format( "this.destFileExt: '%s'", this.destFileExt ), this.verbosity );
                    log( String.format( "this.destFile: '%s'", this.destFile ), this.verbosity );
                    log( String.format( "commentStartString: '%s'", commentStartString ), this.verbosity );
                    log( String.format( "commentEndString: '%s'", commentEndString ), this.verbosity );
                    log( String.format( "Writing to: %s", this.destFile.getName() ), this.verbosity );

                    // System.out.println( String.format( "3:   ================" ) );
                    // System.out.println( String.format( "this.requiredAction: '%s'", this.requiredAction ) );
                    // System.out.println( String.format( "this.destFileExt: '%s'", this.destFileExt ) );
                    // System.out.println( String.format( "this.destFile: '%s'", this.destFile ) );
                    // System.out.println( String.format( "commentStartString: '%s'", commentStartString ) );
                    // System.out.println( String.format( "commentEndString: '%s'", commentEndString ) );
                    // System.out.println( String.format( "Writing to: %s", this.destFile.getName() ) );

                    if ( i == 0 ) {
                        log( String.format( "Processing (%s) %d source file%s%s", this.requiredAction, this.processList.length, (this.processList.length == 1 ? "" : "s"), ( this.destDir != null ? " to " + this.destDir : "" ) ) );
                    }
                    
                    try {
                        long startTime = System.currentTimeMillis();
                        
                        log( String.format( "Action: %s with %s to file %s", this.requiredAction, this.processList[i].getName(), this.rootChunk ), this.verbosity );
                        // Set up the piped components
                        PipedWriter producerPipedWriter = new PipedWriter();
                        PipedReader producerPipedReader = new PipedReader( producerPipedWriter );
                        
                        PipedWriter tagSourcePipedWriter = new PipedWriter();
                        PipedReader tagSourcePipedReader = new PipedReader( tagSourcePipedWriter );
                        
                        PipedWriter tokenizeCodePipedWriter = new PipedWriter();
                        PipedReader tokenizeCodePipedReader = new PipedReader( tokenizeCodePipedWriter );
                        
                        PipedWriter xRefWriter = new PipedWriter();
                        PipedReader xRefReader = new PipedReader( xRefWriter );
                        
                        PipedWriter filterWriter = new PipedWriter();
                        PipedReader filterReader = new PipedReader( filterWriter );
                        
                        PipedWriter fileWriterWriter = new PipedWriter();
                        PipedReader fileWriterReader = new PipedReader( fileWriterWriter );
                        
                        // All actions have the same initial pipeline components
                        // The producer reads the noweb file and injects into the pipeline
                        Thread fileReader = new Thread( new Producer( this.processList[i].toString(), producerPipedWriter ) );
                        // The tagger converts the file into an XML representation
                        Thread tagSourceFilter = new Thread( new TagSource( producerPipedReader, tagSourcePipedWriter, baseName ) );
                        // Mark off the code into tokens to make indexing easier
                        Thread tokenizeCodeFilter = new Thread( new TokenizeCode( tagSourcePipedReader, tokenizeCodePipedWriter ) );
                        // The built in xRefFilter transforms the raw XML into a more suitable noweb one with cross references etc.
                        Thread xRefFilter = new Thread( new XRefFilter( tokenizeCodePipedReader, xRefWriter, filters ) );
                        // Run the filters declared by the default and user pipelines.
                        Thread runFilters = new Thread( new RunFilters( xRefReader, filterWriter, filters, this.delay, this.requiredAction, this.commentStartString, this.commentEndString, this.rootChunk, this.processList[i].getName() ) );
                        // Final part of the pip is to send the results to a file
                        Thread fileWriter = new Thread( new OutputWriter( filterReader, this.destDir.getAbsolutePath(), this.destFile.getName() ) );
                        
                        fileReader.start();
                        tagSourceFilter.start();
                        tokenizeCodeFilter.start();
                        xRefFilter.start();
                        runFilters.start();
                        fileWriter.start();
                        
                        Thread finalThread = fileWriter;
                        long patience = 1000 * 10;
                        while ( finalThread.isAlive() ) {
                            // Wait maximum of 10 seconds for runFilters thread to finish.
                            finalThread.join( 10000 );
                            if ( ( (System.currentTimeMillis() - startTime) > patience ) && finalThread.isAlive() ) {
                                threadMessage( "Thread stuck so exiting!" );
                                finalThread.interrupt();
                                // Wait indefinitely
                                finalThread.join();
                            }
                        }
                        
                    } catch( FileNotFoundException ex ) {
                        throw new BuildException( String.format( "File not found: %s", ex.getMessage() ) );
                    } catch (IOException ex) {
                        String msg = String.format( "Failed to copy %s to %s due to %s", this.processList[i].getName(), this.destDir.getAbsolutePath(), ex.getMessage() );
                        log( msg, Project.MSG_ERR );
                    } catch ( Exception ex ) {
                        log( ex.getMessage(), verbosity );
                        throw new BuildException( ex.getMessage() );
                    }
                }
            }
        } else {
            // Fail path
            this.taskSuccess = false;
            if (errorProperty != null) {
                getProject().setNewProperty( errorProperty, "true" );
            }
            if ( failOnError ) {
                throw new BuildException( Constants.FAIL_MSG, getLocation() );
            } else {
                log( Constants.FAIL_MSG, Project.MSG_ERR );
            }
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Scans the directory looking for noweb files to be processed.
     * The results are returned in the class variable processList
     *
     * @param srcDir   The source directory
     * @param destDir  The destination directory
     * @param files    An array of filenames
     * @param targetExtension    The extension of the destination file (dependent on the action)
     */
    
    protected void scanDir( File srcDir, File destDir, String[] files, String targetExtension )
    {
        GlobPatternMapper m = new GlobPatternMapper();
        String[] extensions = getSupportedFileExtensions();
        //log( String.format( "scanDir: %s", srcDir.getPath() ), this.verbosity );
        //log( String.format( "       : %s", destDir.getPath() ), this.verbosity );
        //log( String.format( "       : '%s'", targetExtension ), this.verbosity );
        
        for ( int i = 0; i < extensions.length; i++ ) {
            m.setFrom( extensions[i] );
            m.setTo( String.format( "*.%s", targetExtension ) );
            SourceFileScanner sfs = new SourceFileScanner( this );
            File[] newFiles = sfs.restrictAsFiles( files, srcDir, destDir, m );
            
            if ( newFiles.length > 0 ) {
                lookForPackageInfos( srcDir, newFiles );
                File[] newCompileList = new File[this.processList.length + newFiles.length];
                System.arraycopy( processList, 0, newCompileList, 0, this.processList.length );
                System.arraycopy( newFiles, 0, newCompileList, this.processList.length, newFiles.length );
                this.processList = newCompileList;
            }
        }
        //log( String.format( "       : finished with %d", this.processList.length ), this.verbosity );
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Get the supported file extension for noweb files.
     *
     * @return a string array of the extensions
     */
    
    private String[] getSupportedFileExtensions()
    {
        String[] extensions = null;
        extensions = new String[] { "nw", "noweb" };
        
        // now process the extensions to ensure that they are the right format
        for (int i = 0; i < extensions.length; i++) {
            if ( !extensions[i].startsWith( String.format( "*%s", Constants.EXTENSION_SEPARATOR ) ) ) {
                extensions[i] = String.format( "*%s%s", Constants.EXTENSION_SEPARATOR, extensions[i] );
            }
        }
        return extensions;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    
    private void lookForPackageInfos( File srcDir, File[] newFiles )
    {
        for (int i = 0; i < newFiles.length; i++) {
            File f = newFiles[i];
            if (!f.getName().equals("package-info.nw")) {
                continue;
            }
            String path = FILE_UTILS.removeLeadingPath(srcDir, f).replace(File.separatorChar, '/');
            String suffix = "/package-info.nw";
            if (!path.endsWith(suffix)) {
                log("anomalous package-info.nw path: " + path, Project.MSG_WARN);
                continue;
            }
            String pkg = path.substring(0, path.length() - suffix.length());
            // packageInfos.put(pkg, new Long(f.lastModified()));
            // System.out.printf( "last modified = '%s'\n", f.lastModified() );
            packageInfos.put( pkg, Long.valueOf( f.lastModified() ) );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the list of files to be compiled.
     *
     * @return the list of files as an array
     */
    
    public File[] getFileList()
    {
        return this.processList;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Gets the file extension from a file name.
     *
     * Not 100% safe if parent folder has embedded separator and the file name does not.
     *
     * @param fileName the filename as string
     * @return the extension as string (or zero string if not found)
     */
    
    private String getExtension( String fileName )
    {
        if ( fileName.isEmpty() ) return "";
        String extension = "";
        int pos = fileName.lastIndexOf( Constants.EXTENSION_SEPARATOR );
        if ( pos > 0  ) {
            extension = fileName.substring( pos + 1 );
        } else {
            extension = "";
        }
        return extension;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Clear the list of files to be weaved/tangled.
     */
    
    private void resetSourceFileLists() {
        this.processList = new File[0];
        this.packageInfos = new HashMap<String, Long>();
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Check that the necessary required attributes have been set.
     *
     * @exception BuildException if an error occurs
     */
    
    private void checkForRequiredAttributes() throws BuildException
    {
        // Check that a source directory has been given and throw exception if none given
        if ( this.srcDir == null ) {
            throw new BuildException( "Source directory must be given", getLocation() );
        }
        if ( this.srcDir.size() == 0 ) {
            throw new BuildException( "Source directory must be given", getLocation() );
        }
    }
    
}



