package uk.org.hsfr.antweave;

import java.io.File;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.BufferedReader;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PipedWriter;
import java.io.InputStream;

import java.util.Enumeration;
import java.util.Random;
import java.util.Map;
import java.util.Hashtable;
import java.util.ListIterator;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.dom.DOMSource;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;

import java.net.URL;

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
// CommentTypes.java
//
// Author
// ------
// Name  : Hugh Field-Richards.
// Email : hsfr@hsfr.org.uk
//
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

class CommentTypes extends DefaultHandler
{
    /** The list of overriding actions */
    private String userCommentTypesFileName = null;
    
    /** The list of start comment characters keyed by the extension string */
    protected Hashtable<String, String> commentStartList = new Hashtable<String, String>();
    
    /** The list of pipelines stored as a table of pipeline filters keyed by the pipeline name */
    protected Hashtable<String, String> commentEndList = new Hashtable<String, String>();
    
    /** Directory where filters are stored */
    private String extensionDirectory = "";
    
    /** Private reference to the default handler instance */
    private CommentTypes contentHandler = null;
    
    /** Internal state for reading the XML file */
    private XMLParseState parsingState = XMLParseState.ROOT;
    
    public enum XMLParseState {
        ROOT,
        TYPE,
    }
    
    // The main instance of this class creates an instance of itself to handle the SAX events. It is this latter
    // instance that stores the lists of pipelines and filters. Care must be taken when accessing the main instance
    // to gather data from there.
    
    private String whichInstance = "";
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * This is constructor when this class is used as the event handler for the Sax parser.
     */
    
    public CommentTypes()
    {
        this.whichInstance = "SAX handler";
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     */
    
    public CommentTypes( String commentsFileName ) throws Exception
    {
        try {
            this.whichInstance = "Extensions handler";
            boolean found = false;
            String javaExtDir = Constants.LOCAL_JAVA_EXTENSIONS_FOLDERS;
            String[] extensionDir = javaExtDir.split( ":" );
            String filterFileName = "";
            
            
            // Search for the default extensions file which should be in same place
            // as user filters: "{java_ext_directory}/antweave"
            for ( int i = 0; i < extensionDir.length; i++ ) {
                String dir = String.format( "%s%santweave", extensionDir[i], Constants.PATH_SEPARATOR );
                // System.out.printf( "dir = '%s'\n", dir );
                filterFileName = String.format( "%s%s%s", dir, Constants.PATH_SEPARATOR, commentsFileName );
                // System.out.printf( "file = '%s'\n", file );
                
                File f = new File( filterFileName );
                if ( f.exists() && !f.isDirectory() ) {
                    this.extensionDirectory = dir;
                    found = true;
                    // System.out.printf( "extensionDir[%d] = '%s'\n", i, this.extensionDirectory );
                    break;
                }
            }
            
            if ( !found ) {
                throw new Exception( "Cannot find extension definitions file" );
            } else {
                // System.out.printf( "Using filters '%s'\n", filterFileName );
            }
            // System.out.printf( "filterFileName = '%s'\n", filterFileName );
            
            // First set up default filter file name
            FileInputStream fis = new FileInputStream( filterFileName );
            InputStream defaultFilterStream = fis;
            
            // InputStream defaultFilterStream = getClass().getResourceAsStream( Constants.DEFAULT_FILTER_FILE_NAME );
            // this.userFilterFileName = userFilterFileName;
            
            // Set up the SAX parser factory
            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setNamespaceAware( true );
            SAXParser saxParser = spf.newSAXParser();
            
            XMLReader xmlReader = saxParser.getXMLReader();
            this.contentHandler = new CommentTypes();
            xmlReader.setContentHandler( this.contentHandler );
            xmlReader.parse( new InputSource( new BufferedInputStream( defaultFilterStream ) ) );
            
            // If the user filter list exists then read it, overwriting the default if necessary (tbd)
            
            // Now transfer all the data gathered by the handler into the main instance. We can do this
            // quite safely because the user and default filter lists are both read only once at
            // startup. We never update them dynamically.
            this.commentStartList = this.contentHandler.commentStartList;
            this.commentEndList = this.contentHandler.commentEndList;
        } catch( Exception e ) {
            System.out.println( "Error creating Filters: " + e );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     *  Does the the comment list contain a named extension.
     *
     * @param ext The extension string
     * @return True if the named pipeline is in the list, otherwise false
     */
    
    public boolean containsExtension( String ext )
    {
        //System.out.printf( "%s: containsPipeline( %s )\n", this.whichInstance, name );
        return this.commentStartList.containsKey( ext );
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     *
     * @param ext The extension string
     * @return start string
     */
    
    public String getStartCommentString( String ext )
    {
        if ( this.containsExtension( ext ) ) {
            return this.commentStartList.get( ext );
        }
        return null;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     *
     * @param ext The extension string
     * @return end string
     */
    
    public String getEndCommentString( String ext )
    {
        if ( this.containsExtension( ext ) ) {
            return this.commentEndList.get( ext );
        }
        return null;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     *  Get the number of entries.
     *
     * @return number of entries
     */
    
    public int numberOfEntries()
    {
        return this.commentStartList.size();
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    
    public String toString()
    {
        String mess = "";
        Hashtable<String, String> startList = this.commentStartList;
        Hashtable<String, String> endList = this.commentEndList;
        String extName = "";
        Enumeration<String> enumKey = startList.keys();
        while( enumKey.hasMoreElements() ) {
            extName = enumKey.nextElement();
            mess += String.format( "Ext: '%s', start: '%s' end: '%s'\n", extName, startList.get(extName), endList.get(extName) );
        }
        return mess;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*- SAX Interface Methods -*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     */
    
    public void startDocument() throws SAXException {
        this.parsingState = XMLParseState.ROOT;
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     */
    
    public void startElement( String namespaceURI, String localName, String qName, Attributes atts ) throws SAXException
    {
        // System.out.printf( "Start element: %s\n", localName );
        if ( localName.equals( Constants.FILE_TYPE_ROOT ) && this.parsingState == XMLParseState.ROOT ) {
            this.parsingState = XMLParseState.TYPE;
        } else if ( localName.equals( Constants.FILE_TYPE_EXT ) ) {
            if ( atts.getLength() == 3 ) {
                String fileExtension = atts.getValue( Constants.EXT_ATTRIBUTE );
                if ( fileExtension != null ) {
                    // We have the key for the two hashtables
                    String startCommentString = atts.getValue( Constants.START_COMMENT_ATTRIBUTE );
                    if ( startCommentString != null ) {
                        // Got a start comment string
                        this.commentStartList.put( fileExtension, startCommentString );
                        String endCommentString = atts.getValue( Constants.END_COMMENT_ATTRIBUTE );
                        if ( endCommentString != null ) {
                            // Got a end comment string
                            this.commentEndList.put( fileExtension, endCommentString );
                        } else {
                            System.out.printf( "Warning: cannot find end comment attribute\n" );
                        }
                    } else {
                        System.out.printf( "Warning: cannot find start comment attribute\n" );
                    }
                } else {
                    System.out.printf( "Warning: cannot find file extension attribute\n" );
                }
            } else {
                System.out.printf( "Warning: cannot find correct number of attributes\n" );
            }
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     */
    
    public void endElement( String namespaceURI, String localName, String qName ) throws SAXException
    {
        // System.out.printf( "End element: %s\n", localName );
        if ( localName.equals( Constants.FILE_TYPE_ROOT ) && this.parsingState == XMLParseState.TYPE ) {
            this.parsingState = XMLParseState.ROOT;
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     */
    
    public void endDocument() throws SAXException {
    }
    
}
