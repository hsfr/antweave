package uk.org.hsfr.antweave;

import java.io.BufferedReader;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import java.io.PipedReader;
import java.io.FileWriter;

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
// OutputWriter.java
//
// Outputs a piped stream to a file.
//
// Author
// ------
// Name  : Hugh Field-Richards.
// Email : hsfr@hsfr.org.uk
//
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

class OutputWriter implements Runnable
{
    private BufferedReader bufferedInputStream = null;
    private BufferedWriter bufferedOutputStream = null;
    
    private String className = "";
    
    /** The destination directory */
    private String destDir = null;
    
    /** The destination file */
    private String destFile = null;
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    
    public OutputWriter( PipedReader inputStream, String destDir, String destFile ) throws Exception
    {
        try {
            this.destDir = destDir;
            this.destFile = destFile;
            String outputFile = String.format( "%s%s%s", this.destDir, Constants.PATH_SEPARATOR, this.destFile );
            this.bufferedInputStream = new BufferedReader( inputStream );
            // STRANGE FIX: I found I needed to set the buffer size to a suitably large figure to cure a problem
            // that was preventing the stream being completely flushed (non-deterministic problem at that).
            this.bufferedOutputStream = new BufferedWriter( new FileWriter( outputFile ), Constants.OUTPUT_BUFFER_SIZE );
            this.className = this.getClass().getSimpleName();
        } catch( Exception e ) {
            System.out.println( "Error creating OutputWriter filter: " + e );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    
    public void run()
    {
        String inputLine = "";
        String outputFile = String.format( "%s%s%s", this.destDir, Constants.PATH_SEPARATOR, this.destFile );
        System.out.printf( "Output to %s\n", outputFile );
        
        try {
            while ( ( inputLine = this.bufferedInputStream.readLine() ) != null ) {
                this.bufferedOutputStream.write( String.format( "%s\n", inputLine ) );
            }
            //System.out.printf( "Finished writing to %s\n", this.destFile );
            this.bufferedOutputStream.flush();
            this.bufferedOutputStream.close();
            this.bufferedInputStream.close();
        } catch( Exception e ) {
            System.out.println( "OutputWriter: Error: " + e); 
        }
    } 
}
