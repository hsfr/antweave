package uk.org.hsfr.antweave;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Hashtable;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PipedReader;
import java.io.PipedWriter;

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
// PatternInfo.java
//
// A Utility class to return tuple values from search method.
//
// Author
// ------
// Name  : Hugh Field-Richards.
// Email : hsfr@hsfr.org.uk
//
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/**
* A Utility class to return tuple values from search method.
*/

class PatternInfo {
  
        public boolean found = false;
        public String[] pattern = new String[5];  // index 0 not used!
        
        public PatternInfo( boolean f )
        {
            this.found = f;
            this.pattern[1] = "";
            this.pattern[2] = "";
            this.pattern[3] = "";
            this.pattern[4] = "";
        }
        
        public PatternInfo( boolean f, String p )
        {
            this.found = f;
            this.pattern[1] = p;
            this.pattern[2] = "";
            this.pattern[3] = "";
            this.pattern[4] = "";
        }
        
        public PatternInfo( boolean f, String p1, String p2 )
        {
            this.found = f;
            this.pattern[1] = p1;
            this.pattern[2] = p2;
            this.pattern[3] = "";
            this.pattern[4] = "";
        }
        
        public PatternInfo( boolean f, String p1, String p2, String p3 )
        {
            this.found = f;
            this.pattern[1] = p1;
            this.pattern[2] = p2;
            this.pattern[3] = p3;
            this.pattern[4] = "";
        }
        
        public PatternInfo( boolean f, String p1, String p2, String p3, String p4 )
        {
            this.found = f;
            this.pattern[1] = p1;
            this.pattern[2] = p2;
            this.pattern[3] = p3;
            this.pattern[4] = p4;
        }
}




