package uk.org.hsfr.antweave;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import java.io.IOException;
import java.io.Writer;
import java.io.StringWriter;

import java.util.ArrayList;
import java.util.ListIterator;

import java.net.URL;

import java.lang.ClassLoader;

import javax.xml.parsers.DocumentBuilder;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.Result;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.OutputKeys;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
// RunFilters.java
//
// This class runs the filters found in the filters.xml file (or user one if wanted and present)
//
// Author
// ------
// Name  : Hugh Field-Richards.
// Email : hsfr@hsfr.org.uk
//
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

class RunFilters implements Runnable
{
    private BufferedWriter bufferedOutputStream = null;
    private BufferedReader bufferedInputStream = null;
    private String antweaveFileName = "";
    private String commentStartString = "//";
    private String commentEndString = "";
    private boolean delayedWrapper = false;
    private String rootChunk = "";
    private Filters filters = null;
    private String requiredAction = "tangle";
    
    private String className = "";
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Set up the run filters class.
     *
     * @param inputStream The input from the previous stage of the pipeline
     * @param outputStream The output stream to the next stage of the pipeline
     * @param filters The list of filters that we have to invoke
     * @param delayedWrapper should the weave TeX output delay the TeX wrapper (see documentation)
     * @param requiredAction The required action to pick up the appropriate pipeline
     * @param commentStartString The characters that should start a comment in the tangle output for cross referencing
     * @param commentEndString The characters that should be used to close a comment
     * @param rootChunk The root chunk from where everything is built
     * @param antweaveFileName The name of the original noweb source file being processed
     * @exception Exception if problem creating the producer
     */
    
    public RunFilters( PipedReader inputStream,
                      PipedWriter outputStream,
                      Filters filters,
                      boolean delayedWrapper,
                      String requiredAction,
                      String commentStartString,
                      String commentEndString,
                      String rootChunk,
                      String antweaveFileName ) throws Exception
    {
        try {
            this.bufferedInputStream = new BufferedReader( inputStream );
            this.bufferedOutputStream = new BufferedWriter( outputStream );
            this.filters = filters;
            this.delayedWrapper = delayedWrapper;
            this.requiredAction = requiredAction;
            this.commentStartString = commentStartString;
            this.commentEndString = commentEndString;
            this.rootChunk = rootChunk;
            this.antweaveFileName = antweaveFileName;
            this.className = this.getClass().getSimpleName();
            //System.out.printf( "%s: created\n", this.className );
        } catch ( Exception e ) {
            System.out.printf( "Error creating %s filter: %s\n", this.className, e.toString() );
        }
    }
    
    //-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    /**
     * Runs the GenerateCode thread.
     */
    
    public void run()
    {
        /* System.out.printf( "%s:\n", this.className );
         System.out.printf( "    this.rootChunk: %s\n", this.rootChunk );
         System.out.printf( "    this.requiredAction: %s\n", this.requiredAction );
         System.out.printf( "    this.delayedWrapper: %s\n", this.delayedWrapper ? "yes" : "no" );
         System.out.printf( "    this.antweaveFileName: %s\n", this.antweaveFileName );
         System.out.printf( "    this.commentStartString: %s\n", this.commentStartString );
         System.out.printf( "    this.commentEndString: %s\n", this.commentEndString );
         System.out.printf( "%s\n", this.filters.toString() ); */
        
        int filterIndex = 1;
        int numberOfFilters = 0;
        String filterFileName = "";
        String transformFileName = "";
        FileInputStream transformInputStream = null;
        InputStream XSTLInputStream = null;
        TransformerFactory transformerFactory = null;
        StreamSource XSLTFileStreamSource = null;
        Transformer transformer = null;
        
        // Does the required action exist in the filters list
        try {
            if ( this.filters.containsPipeline( this.requiredAction ) ) {
                
                // Get the input stream and convert to a DOM
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                dbf.setValidating( false );
                dbf.setCoalescing( false );
                dbf.setIgnoringComments( false );
                dbf.setIgnoringElementContentWhitespace( false );
                dbf.setNamespaceAware( true );
                
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document pipelinedDocument = db.parse( new InputSource( this.bufferedInputStream ) );
                // Constants.prettyPrint( pipelinedDocument );
                
                ArrayList<String> filtersList = this.filters.filterList( this.requiredAction );
                numberOfFilters = this.filters.numberOfFilters( this.requiredAction );
                // System.out.printf( "%s: Running filter pipeline '%s' with %d filter(s) found\n", this.className, this.requiredAction, numberOfFilters );
                
                for ( ListIterator<String> e = filtersList.listIterator(); e.hasNext(); ) {
                    filterFileName = e.next();
                    // System.out.printf( "%s: Running filter %d '%s'\n", this.className, filterIndex, filterFileName );
                    
                    // filterFileNames are absolute path anf file name
                    transformFileName = String.format( "%s", filterFileName );
                    // System.out.printf( "%s: transformFileName: %s\n", this.className, transformFileName );
                    // Set up the input stream for the transformer
                    transformInputStream = new FileInputStream( transformFileName );
                    XSTLInputStream = transformInputStream;
                    // Set up the transformer factory and associate it with the XSL stream
                    transformerFactory = TransformerFactory.newInstance();
                    XSLTFileStreamSource = new StreamSource( XSTLInputStream );
                    transformer = transformerFactory.newTransformer( XSLTFileStreamSource );
                    // These are the default built-in set. Ultimately there will be ones taken from the filters.xml file (or user's one)
                    transformer.setParameter( "startCommentString", this.commentStartString );
                    transformer.setParameter( "endCommentString", this.commentEndString );
                    transformer.setParameter( "rootChunk", this.rootChunk );
                    transformer.setParameter( "delay", this.delayedWrapper ? "yes" : "no" );
                    transformer.setParameter( "antweaveFileName", this.antweaveFileName );
                    // Set up the source from the pipelinedDocument
                    DOMSource source = new DOMSource( pipelinedDocument );
                    // Constants.prettyPrint( pipelinedDocument );
                    
                    // If not last filter
                    if ( numberOfFilters != filterIndex ) {
                        // Set up the result which will become a new Document instance
                        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                        Document doc = builder.newDocument();
                        Result result = new DOMResult( doc );
                        
                        transformer.transform( source, result );
                        pipelinedDocument = doc;
                        // Constants.prettyPrint( pipelinedDocument );
                        filterIndex++;
                    } else {  // run last filter
                        StreamResult result = new StreamResult( this.bufferedOutputStream );
                        transformer.transform( source, result );
                        this.bufferedOutputStream.close();
                    }
                }
                // System.out.printf( "%s: Finished\n", this.className );
            } else {
                System.out.printf( "Cannot find action '%s' in filters list\n", this.requiredAction );
            }
        } catch ( SAXException sxe ) {
            // Error generated by this application (or a parser-initialization error)
            Exception x = sxe;
            if (sxe.getException() != null) {
                x = sxe.getException();
            }
            x.printStackTrace();
        } catch ( ParserConfigurationException pce ) {
            // Parser with specified options can't be built
            pce.printStackTrace();
        } catch (IOException ioe) {
            // I/O error
            ioe.printStackTrace();
        } catch( Exception e ) {
            System.out.printf( "%s error: %s\n", this.className, e.toString() );
        }
        
    }
    
}



