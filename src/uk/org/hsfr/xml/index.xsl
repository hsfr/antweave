<?xml version="1.0" encoding="UTF-8"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--

   Tagged noweb to weave Transform

   Author:
	   Name  : Hugh Field-Richards
	   Email : hsfr@hsfr.org.uk

-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   Date                   Who    Changes
   ==========================================================================
   14th July 2015         HSFR   Created

-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Copyright -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   All copyright, database rights and other intellectual property in this
   software is the property of Hugh Field-Richards. The software is being
   released free of charge for use in accordance with the terms of open
   source initiative GNU General Public Licence.
      
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* Disclaimer -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   The author and copyright holder does not verify or warrant the accuracy,
   completeness or suitability of this software for any particular purpose.
   Any use of this software whatsoever is entirely at the user's own risk
   and the author and copyright holder accepts no liability in relation thereto.
                  
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:aw="http://www.hsfr.org.uk/Schema/AntWeave" version="1.0">

   <xsl:output version="1.0" encoding="UTF-8" indent="no"/>

   <xsl:preserve-space elements="*"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	-->

   <xsl:template match="/">
      <xsl:element name="aw:file">
         <xsl:apply-templates mode="process-chunks"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*- DOCUMENTATION CHUNK -*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	-->

   <xsl:template match="aw:chunk[@type = 'documentation']" mode="process-chunks">
      <xsl:element name="aw:chunk">
         <xsl:copy-of select="@*"/>
         <xsl:apply-templates mode="process-documentation-chunk"/>
      </xsl:element>
   </xsl:template>

   <!-- 
      Pass through anything in the documentation chunk. This will be may be LaTeX/TeX
      or HTML etc so we have to be transparent here marking everything as CDATA.
    -->

   <xsl:template match="node() | @*" mode="process-documentation-chunk">
      <xsl:copy>
         <xsl:apply-templates select="@*" mode="process-documentation-chunk"/>
         <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
         <xsl:apply-templates mode="process-documentation-chunk"/>
         <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
      </xsl:copy>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*- DEFINITIONS CHUNK -*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Definitions chunks include both the list of individually declared (%def)
      tokens and also where the included chunks within chunks are used.
	-->

   <xsl:template match="aw:definitions" mode="process-chunks">
      <xsl:element name="aw:definitions">
         <xsl:copy-of select="@*"/>
         <xsl:apply-templates mode="process-definition-chunk"/>
      </xsl:element>
   </xsl:template>

   <!-- 
       Pass through everything in the definition chunk. Subsequent filters will make
       use of this.
    -->

   <xsl:template match="node() | @*" mode="process-definition-chunk">
      <xsl:copy>
         <xsl:apply-templates select="@*" mode="process-definition-chunk"/>
         <xsl:apply-templates mode="process-definition-chunk"/>
      </xsl:copy>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- CODE CHUNK *-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      This is where the work is done. This filter trawls through the defines
      and marks up any token that has been defined
	-->

   <xsl:template match="aw:chunk[@type = 'code']" mode="process-chunks">
      <xsl:element name="aw:chunk">
         <xsl:copy-of select="@*"/>
         <xsl:apply-templates mode="process-code-chunk"/>
      </xsl:element>
   </xsl:template>

   <!--
      We scan through the entire list of code chunks looking for defines and marking 
      the tokens accordingly.
	-->

   <xsl:template match="aw:code" mode="process-code-chunk">
      <xsl:element name="aw:code">
         <xsl:copy-of select="@*"/>
         <xsl:apply-templates mode="process-token"/>
      </xsl:element>
   </xsl:template>

   <!--

      If token has been defined then output it as a variable in the line.
	-->

   <xsl:template match="aw:token" mode="process-token">
      <xsl:variable name="token" select="."/>

      <xsl:choose>
         <xsl:when test="count(//aw:definitions/aw:define[@var = $token]) != 0">
            <xsl:element name="aw:variable">
               <xsl:value-of disable-output-escaping="yes" select="$token"/>
            </xsl:element>
         </xsl:when>
         <xsl:otherwise>
            <xsl:copy-of select="@*"/>
            <xsl:copy-of select="."/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!--
      Pass through all the code separators.
	-->

   <xsl:template match="aw:separator" mode="process-token">
      <xsl:copy-of select="@*"/>
      <xsl:copy-of select="."/>
   </xsl:template>

   <!--
      Include statements are just passed through with no change
	-->

   <xsl:template match="aw:include" mode="process-token">
      <xsl:copy-of select="@*"/>
      <xsl:copy-of select="."/>
   </xsl:template>

   <!-- 
      Outputs the text with suitable CDATA.
    -->

   <xsl:template match="text()" mode="process-token">
      <xsl:value-of disable-output-escaping="yes" select="concat('&lt;![CDATA[', ., ']]&gt;')"/>
   </xsl:template>

</xsl:stylesheet>
