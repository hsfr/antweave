<?xml version="1.0" encoding="UTF-8"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--

  Weave Transform

   Author:
	   Name  : Hugh Field-Richards
	   Email : hsfr@hsfr.org.uk

-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   Date                   Who    Changes
   ==========================================================================
   14th July 2015         HSFR   Created
   10th February 2016     HSFR   major fixes to indexing and normalising variables
   
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Copyright -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   All copyright, database rights and other intellectual property in this
   software is the property of Hugh Field-Richards. The software is being
   released free of charge for use in accordance with the terms of open
   source initiative GNU General Public Licence.
      
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* Disclaimer -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   The author and copyright holder does not verify or warrant the accuracy,
   completeness or suitability of this software for any particular purpose.
   Any use of this software whatsoever is entirely at the user's own risk
   and the author and copyright holder accepts no liability in relation thereto.
                  
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:aw="http://www.hsfr.org.uk/Schema/AntWeave" version="1.0">

   <xsl:output version="1.0" method="text" standalone="yes" omit-xml-declaration="yes" encoding="UTF-8" indent="no"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Add any parameters from the calling program here.
   -->

   <xsl:param name="antweaveFileName" select="''"/>
   <xsl:param name="startCommentString" select="'%'"/>
   <xsl:param name="endCommentString" select="''"/>
   <xsl:param name="delay" select="'no'"/>
   <xsl:param name="index" select="'yes'"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Global constants.
   -->

   <xsl:variable name="NEW_LINE" select="'&#xA;'"/>
   <xsl:variable name="NON_BREAKING_SPACE" select="'~'"/>
   <xsl:variable name="SPACE" select="' '"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xsl:template match="/">

      <xsl:choose>
         <xsl:when test="$delay = 'no'">
            <xsl:call-template name="output-noweb-latex-header"/>
            <xsl:call-template name="process-chunks">
               <xsl:with-param name="start-chunk" select="'1'"/>
            </xsl:call-template>
            <xsl:call-template name="output-latex-footer"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="output-first-documentation-chunk"/>
            <xsl:call-template name="output-antweaveFileName"/>
            <xsl:call-template name="process-chunks">
               <xsl:with-param name="start-chunk" select="'2'"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	-->

   <xsl:template name="output-noweb-latex-header">
      <xsl:value-of select="'\documentclass{article}'"/>
      <xsl:value-of select="'\usepackage{noweb}'"/>
      <xsl:value-of select="'\pagestyle{noweb}'"/>
      <xsl:value-of select="'\noweboptions{}'"/>
      <xsl:value-of select="'\begin{document}'"/>
      <xsl:call-template name="output-antweaveFileName"/>
   </xsl:template>

   <xsl:template name="output-latex-footer">
      <xsl:value-of select="concat('\end{document}', $NEW_LINE)"/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	-->

   <xsl:template name="output-antweaveFileName">
      <xsl:value-of select="concat('\nwfilename{', $antweaveFileName, '}')"/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
   -->

   <xsl:template name="output-first-documentation-chunk">
      <xsl:choose>
         <xsl:when test="//aw:chunk[position() = '1' and @type = 'documentation']">
            <xsl:for-each select="//aw:chunk[@type = 'documentation'][position() = '1']/aw:text">

               <xsl:variable name="removeStart">
                  <xsl:choose>
                     <xsl:when test="starts-with(., '&lt;![CDATA[')">
                        <xsl:value-of select="substring-after(., '&lt;![CDATA[')"/>
                     </xsl:when>
                     <xsl:otherwise>
                        <xsl:value-of select="."/>
                     </xsl:otherwise>
                  </xsl:choose>
               </xsl:variable>

               <xsl:variable name="removeEnd">
                  <xsl:choose>
                     <xsl:when test="contains($removeStart, ']]&gt;') and not(substring-after($removeStart, ']]&gt;'))">
                        <xsl:value-of select="substring-before($removeStart, ']]&gt;')"/>
                     </xsl:when>
                     <xsl:otherwise>
                        <xsl:value-of select="$removeStart"/>
                     </xsl:otherwise>
                  </xsl:choose>
               </xsl:variable>

               <xsl:value-of disable-output-escaping="yes" select="concat($removeEnd, $NEW_LINE)"/>

            </xsl:for-each>
         </xsl:when>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	-->

   <xsl:template name="process-chunks">
      <xsl:param name="start-chunk"/>

      <xsl:for-each select="//aw:chunk[position() >= $start-chunk]">
         <xsl:choose>
            <xsl:when test="@type = 'documentation'">
               <xsl:if test="position() = last() and $index = 'yes'">
                  <xsl:call-template name="output-final-index"/>
               </xsl:if>
               <xsl:call-template name="begin-documentation-section"/>
               <xsl:call-template name="nwdocspar"/>
               <xsl:call-template name="output-documentation-lines"/>
               <xsl:call-template name="end-documentation-section"/>
            </xsl:when>
            <xsl:when test="@type = 'code'">
               <xsl:call-template name="begin-code-section"/>
               <xsl:if test="$index = 'yes'">
                  <xsl:call-template name="sublabel"/>
                  <xsl:call-template name="nwmargintag"/>
               </xsl:if>
               <xsl:call-template name="moddef"/>
               <xsl:value-of select="'\nwstartdeflinemarkup'"/>
               <xsl:call-template name="output-usesondefline"/>
               <xsl:call-template name="output-prevnextdefs"/>
               <xsl:value-of select="'\nwenddeflinemarkup'"/>
               <xsl:apply-templates mode="output-code"/>
               <xsl:call-template name="output-definitions-usedIn"/>
               <xsl:call-template name="end-code-section"/>
            </xsl:when>
         </xsl:choose>
      </xsl:for-each>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
       Mark beginnning of documentation section.
	-->

   <xsl:template name="begin-documentation-section">
      <xsl:value-of select="'\nwbegindocs{'"/>
      <xsl:value-of select="position() - 1"/>
      <xsl:value-of select="'}'"/>
   </xsl:template>

   <!-- 
       Mark end of documentation section.
	-->

   <xsl:template name="end-documentation-section">
      <xsl:value-of select="'\nwenddocs{'"/>
      <xsl:value-of select="'}'"/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* CODE CHUNK -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
       Mark beginnning of code section.
	-->

   <xsl:template name="begin-code-section">
      <xsl:value-of select="concat('\nwbegincode{', position() - 1, '}')"/>
   </xsl:template>

   <!-- 
       Mark end of code section.
	-->

   <xsl:template name="end-code-section">
      <xsl:value-of select="'\nwendcode{}'"/>
   </xsl:template>

   <!-- 
       Output the various definition, usedIn and continuations.       
	-->

   <xsl:template name="output-definitions-usedIn">
      <xsl:param name="codeHash"/>

      <xsl:call-template name="output-defines-for-index"/>
      <xsl:call-template name="output-also-defined-in"/>
      <xsl:call-template name="output-used-in"/>
      <xsl:call-template name="output-defines"/>
      <xsl:call-template name="output-variables-used"/>
      <xsl:call-template name="output-index-used"/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-* DEFINES FOR INDEX SECTION *-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Output the defines section
   -->

   <xsl:template name="output-defines-for-index">
      <xsl:variable name="currentChunkHash" select="@hash"/>

      <xsl:for-each select="//aw:definitions[@chunk = $currentChunkHash]/aw:define">

         <xsl:variable name="normalisedVar1">
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="@var"/>
            </xsl:call-template>
         </xsl:variable>

         <xsl:variable name="normalisedVar2">
            <xsl:call-template name="normalisedVarInIndex">
               <xsl:with-param name="text" select="@var"/>
            </xsl:call-template>
         </xsl:variable>

         <xsl:variable name="variableDef">
            <xsl:value-of select="concat('\nwixident{', $normalisedVar1, '}')"/>
         </xsl:variable>

         <xsl:variable name="variablePair">
            <xsl:value-of select="concat('{', $normalisedVar2, '}{', $currentChunkHash, '}')"/>
         </xsl:variable>

         <xsl:value-of select="concat('\nwindexdefn{', $variableDef, '}', $variablePair)"/>
      </xsl:for-each>
      <xsl:if test="//aw:definitions[@chunk = $currentChunkHash]/aw:define">
         <xsl:value-of select="concat('\eatline', $NEW_LINE)"/>
      </xsl:if>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-* DEFINES SECTION *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Output the defines section
   -->

   <xsl:template name="output-defines">
      <xsl:variable name="currentChunkHash" select="@hash"/>

      <xsl:variable name="defList">
         <xsl:for-each select="//aw:definitions[@chunk = $currentChunkHash]/aw:define">

            <xsl:variable name="normalisedVar1">
               <xsl:call-template name="normalisedTeXInDocumentation">
                  <xsl:with-param name="text" select="@var"/>
               </xsl:call-template>
            </xsl:variable>

            <xsl:variable name="normalisedVar2">
               <xsl:call-template name="normalisedVarInIndex">
                  <xsl:with-param name="text" select="@var"/>
               </xsl:call-template>
            </xsl:variable>

            <xsl:value-of select="concat('\\{{\nwixident{', $normalisedVar1, '}}{', $normalisedVar2, '}}')"/>
         </xsl:for-each>
      </xsl:variable>

      <xsl:if test="string-length($defList) > 0">
         <xsl:value-of select="concat('\nwidentdefs{', $defList, '}')"/>
      </xsl:if>

   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-* ALSO DEFINED IN SECTION *-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Output the also defined in chunk section. Used when two chunks have the same name.
   -->

   <xsl:template name="output-also-defined-in">
      <xsl:variable name="currentChunkHash" select="@hash"/>
      <xsl:variable name="currentChunkId">
         <xsl:call-template name="substring-before-last">
            <xsl:with-param name="delimiter" select="'-'"/>
            <xsl:with-param name="string" select="$currentChunkHash"/>
         </xsl:call-template>
      </xsl:variable>

      <!--
         Now trawl through all the chunks to see whether there are any other with the same name.
      -->

      <xsl:variable name="also-list">
         <xsl:for-each select="//aw:chunk[@type = 'code']">
            <xsl:variable name="chunkId">
               <xsl:call-template name="substring-before-last">
                  <xsl:with-param name="delimiter" select="'-'"/>
                  <xsl:with-param name="string" select="@hash"/>
               </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="chunkIndex">
               <xsl:call-template name="substring-after-last">
                  <xsl:with-param name="delimiter" select="'-'"/>
                  <xsl:with-param name="string" select="@hash"/>
               </xsl:call-template>
            </xsl:variable>
            <xsl:if test="($chunkId = $currentChunkId) and $chunkIndex != 1">
               <xsl:value-of select="concat('\\{', @hash, '}')"/>
            </xsl:if>
         </xsl:for-each>
      </xsl:variable>

      <xsl:if test="string-length($also-list) > 0">
         <xsl:value-of select="concat('\nwalsodefined{', $also-list, '}')"/>
      </xsl:if>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-* VARIABLES TO BE INDEXED *-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Which variables are used in this associated chunk.
 	-->

   <xsl:template name="output-index-used">
      <xsl:variable name="currentHash" select="@hash"/>

      <!-- Get a list variables in this chunk -->
      <xsl:variable name="usedList">
         <xsl:for-each select="//aw:chunk[@hash = $currentHash]/aw:code/aw:variable">
            <xsl:value-of select="concat(., ' ')"/>
         </xsl:for-each>
      </xsl:variable>

      <xsl:variable name="normalisedUsedList">
         <xsl:call-template name="remove-duplicates">
            <xsl:with-param name="delimiter" select="' '"/>
            <xsl:with-param name="newstring" select="''"/>
            <xsl:with-param name="string" select="$usedList"/>
         </xsl:call-template>
      </xsl:variable>

      <xsl:variable name="taggedUsedList">
         <xsl:call-template name="build-indexed-variables">
            <xsl:with-param name="newstring" select="''"/>
            <xsl:with-param name="delimiter" select="' '"/>
            <xsl:with-param name="string" select="$normalisedUsedList"/> * <xsl:with-param name="currentHash" select="$currentHash"/>
         </xsl:call-template>
      </xsl:variable>

      <xsl:value-of select="$taggedUsedList"/>

   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
     This recursive routine takes a list of variables used in the current chunk and
      returns a suitably encoded TeX string. For example if the string is
      
      "j k"
      
      it returns 
      
         \nwindexuse{\nwixident{j}}{j}{NW48ed0e68-205ec52-2}\nwindexuse{\nwixident{k}}{k}{NW48ed0e68-205ec52-2}
      
      unless this is where k is defined when it will return
      
         \nwindexuse{\nwixident{j}}{j}{NW48ed0e68-205ec52-2}
      
     -->

   <xsl:template name="build-indexed-variables">
      <xsl:param name="string"/>
      <xsl:param name="newstring"/>
      <xsl:param name="delimiter"/>
      <xsl:param name="currentHash"/>

      <xsl:choose>
         <!--
            When string being processed is empty we have finished so the new string
            should contain what we are interested in.
         -->
         <xsl:when test="$string = ''">
            <xsl:value-of select="$newstring"/>
         </xsl:when>

         <!--
            Still things to process
         -->
         <xsl:otherwise>

            <xsl:variable name="nextToken">
               <xsl:choose>
                  <xsl:when test="contains($string, $delimiter)">
                     <!-- If string contains a delimiter we set nextToken to first token (before delimiter) -->
                     <xsl:value-of select="substring-before($string, $delimiter)"/>
                  </xsl:when>
                  <xsl:otherwise>
                     <!-- If no delimiter then nextToken is whole string -->
                     <xsl:value-of select="$string"/>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:variable>

            <xsl:variable name="normalisedVar1">
               <xsl:call-template name="normalisedTeXInDocumentation">
                  <xsl:with-param name="text" select="$nextToken"/>
               </xsl:call-template>
            </xsl:variable>

            <xsl:variable name="normalisedVar2">
               <xsl:call-template name="normalisedVarInIndex">
                  <xsl:with-param name="text" select="$nextToken"/>
               </xsl:call-template>
            </xsl:variable>

            <xsl:variable name="remainderLine" select="substring-after($string, $delimiter)"/>

            <!-- Is the nextToken defined in this current chunk? -->
            <xsl:choose>
               <xsl:when test="//aw:definitions[@chunk = $currentHash]//aw:define[@var = $nextToken]">
                  <!-- Yes so ignore it and recurse -->
                  <xsl:call-template name="build-indexed-variables">
                     <xsl:with-param name="string" select="$remainderLine"/>
                     <xsl:with-param name="newstring" select="$newstring"/>
                     <xsl:with-param name="delimiter" select="$delimiter"/>
                     <xsl:with-param name="currentHash" select="$currentHash"/>
                  </xsl:call-template>
               </xsl:when>
               <xsl:otherwise>
                  <!-- Build new string into temporary variable -->
                  <xsl:variable name="temp">
                     <xsl:choose>
                        <xsl:when test="$newstring = ''">
                           <xsl:value-of select="concat('\nwindexuse{\nwixident{', $normalisedVar1, '}}{', $normalisedVar2, '}{', $currentHash, '}')"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:value-of
                              select="concat($newstring, '\nwindexuse{\nwixident{', $normalisedVar1, '}}{', $normalisedVar2, '}{', $currentHash, '}')"/>
                        </xsl:otherwise>
                     </xsl:choose>
                  </xsl:variable>

                  <xsl:call-template name="build-indexed-variables">
                     <xsl:with-param name="string" select="$remainderLine"/>
                     <xsl:with-param name="newstring" select="$temp"/>
                     <xsl:with-param name="delimiter" select="$delimiter"/>
                     <xsl:with-param name="currentHash" select="$currentHash"/>
                  </xsl:call-template>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-* WHERE CHUNK IS USED *-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Output the "Used in" section. Current node is the current chunk we are processing.
   -->

   <xsl:template name="output-used-in">
      <xsl:variable name="currentChunkHash" select="@hash"/>

      <xsl:choose>
         <xsl:when test="not(//aw:definitions[@chunk = $currentChunkHash]/aw:usedIn/aw:where)">
            <xsl:value-of select="concat('\nwnotused{', @name, '}')"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:variable name="whereUsed">
               <xsl:for-each select="//aw:definitions[@chunk = $currentChunkHash]/aw:usedIn/*">
                  <xsl:variable name="whereUsedIndex" select="@ref"/>
                  <xsl:value-of select="concat('\\{', //aw:chunk[@id = $whereUsedIndex]/@hash, '}')"/>
               </xsl:for-each>
            </xsl:variable>
            <xsl:value-of select="concat('\nwused{', $whereUsed, '}')"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-* VARIABLES USED IN CHUNK *-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Which variables are used in this associated chunk.
	-->

   <xsl:template name="output-variables-used">
      <xsl:variable name="currentChunkHash" select="@hash"/>

      <xsl:variable name="usedList">
         <xsl:for-each select="//aw:chunk[@hash = $currentChunkHash]/aw:code/aw:variable">
            <xsl:value-of select="concat(., ' ')"/>
         </xsl:for-each>
      </xsl:variable>

      <xsl:variable name="normalisedUsedList">
         <xsl:call-template name="remove-duplicates">
            <xsl:with-param name="delimiter" select="' '"/>
            <xsl:with-param name="newstring" select="''"/>
            <xsl:with-param name="string" select="$usedList"/>
         </xsl:call-template>
      </xsl:variable>

      <xsl:variable name="taggedUsedList">
         <xsl:call-template name="build-variables">
            <xsl:with-param name="newstring" select="''"/>
            <xsl:with-param name="delimiter" select="' '"/>
            <xsl:with-param name="string" select="$normalisedUsedList"/>
            <xsl:with-param name="currentHash" select="$currentChunkHash"/>
         </xsl:call-template>
      </xsl:variable>

      <xsl:if test="string-length($taggedUsedList) > 0">
         <xsl:value-of select="concat('\nwidentuses{', $taggedUsedList, '}')"/>
      </xsl:if>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      This recursive routine takes a list of variables used in the current chunk and
      returns a suitably encoded TeX string. For example if the string is
      
      "j k"
      
      it returns 
      
         \nwidentuses{\\{{\nwixident{j}}{j}}\\{{\nwixident{k}}{k}}}
      
      unless this is where k is defined when it will return
      
         \nwidentuses{\\{{\nwixident{j}}{j}}}
      
     -->

   <xsl:template name="build-variables">
      <xsl:param name="string"/>
      <xsl:param name="newstring"/>
      <xsl:param name="delimiter"/>
      <xsl:param name="currentHash"/>

      <xsl:choose>
         <!--
            When string being processed is empty we have finished so the new string
            should contain what we are interested in.
         -->
         <xsl:when test="$string = ''">
            <xsl:value-of select="$newstring"/>
         </xsl:when>

         <!--
            Still things to process
         -->
         <xsl:otherwise>

            <xsl:variable name="nextToken">
               <xsl:choose>
                  <xsl:when test="contains($string, $delimiter)">
                     <!-- If string contains a delimiter we set nextToken to first token (before delimiter) -->
                     <xsl:value-of select="substring-before($string, $delimiter)"/>
                  </xsl:when>
                  <xsl:otherwise>
                     <!-- If no delimiter then nextToken is whole string -->
                     <xsl:value-of select="$string"/>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:variable>

            <xsl:variable name="normalisedVar1">
               <xsl:call-template name="normalisedTeXInDocumentation">
                  <xsl:with-param name="text" select="$nextToken"/>
               </xsl:call-template>
            </xsl:variable>

            <xsl:variable name="normalisedVar2">
               <xsl:call-template name="normalisedVarInIndex">
                  <xsl:with-param name="text" select="$nextToken"/>
               </xsl:call-template>
            </xsl:variable>

            <xsl:variable name="remainderLine" select="substring-after($string, $delimiter)"/>

            <!-- Is the nextToken defined in this current chunk? -->
            <xsl:choose>
               <xsl:when test="//aw:definitions[@chunk = $currentHash]//aw:define[@var = $nextToken]">
                  <!-- Yes so ignore it and recurse -->
                  <xsl:call-template name="build-variables">
                     <xsl:with-param name="string" select="$remainderLine"/>
                     <xsl:with-param name="newstring" select="$newstring"/>
                     <xsl:with-param name="delimiter" select="$delimiter"/>
                     <xsl:with-param name="currentHash" select="$currentHash"/>
                  </xsl:call-template>
               </xsl:when>

               <xsl:otherwise>
                  <!-- Build new string into temporary variable -->
                  <xsl:variable name="temp">
                     <xsl:choose>
                        <xsl:when test="$newstring = ''">
                           <xsl:value-of select="concat('\\{{\nwixident{', $normalisedVar1, '}}{', $normalisedVar2, '}}')"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:value-of select="concat($newstring, '\\{{\nwixident{', $normalisedVar1, '}}{', $normalisedVar2, '}}')"/>
                        </xsl:otherwise>
                     </xsl:choose>
                  </xsl:variable>

                  <xsl:call-template name="build-variables">
                     <xsl:with-param name="string" select="$remainderLine"/>
                     <xsl:with-param name="newstring" select="$temp"/>
                     <xsl:with-param name="delimiter" select="$delimiter"/>
                     <xsl:with-param name="currentHash" select="$currentHash"/>
                  </xsl:call-template>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:otherwise>
      </xsl:choose>

   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* CHUNKS USAGE -*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      This marks off where each chunk is used.
	-->

   <xsl:template name="output-usesondefline">
      <xsl:variable name="currentChunkName" select="@name"/>

      <xsl:variable name="hashList">
         <!-- Go through each chunk looking for include statements that are the current chunk -->
         <xsl:for-each select="//aw:chunk[@type = 'code']">
            <xsl:variable name="usedInHash" select="@hash"/>

            <!-- In each chunk look at the includes to see whether the name is the same as the current chunk name -->
            <xsl:for-each select="aw:code/aw:include">
               <xsl:if test="@ref = $currentChunkName">
                  <xsl:value-of select="concat('\\{', $usedInHash, '}')"/>
               </xsl:if>
            </xsl:for-each>
         </xsl:for-each>
      </xsl:variable>

      <xsl:if test="string-length($hashList) > 0">
         <xsl:value-of select="concat('\nwusesondefline{', $hashList, '}')"/>
      </xsl:if>

   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-* CHUNKS PREVIOUS AND NEXT USAGE -*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      This marks off previous and next usage for chunks.
	-->

   <xsl:template name="output-prevnextdefs">
      <xsl:variable name="currentName" select="@name"/>

      <!-- Final digits of current hash -->
      <xsl:variable name="hashIndex">
         <xsl:call-template name="substring-after-last">
            <xsl:with-param name="delimiter" select="'-'"/>
            <xsl:with-param name="string" select="@hash"/>
         </xsl:call-template>
      </xsl:variable>

      <xsl:variable name="chunkHashWithoutIndex">
         <xsl:call-template name="substring-before-last">
            <xsl:with-param name="delimiter" select="'-'"/>
            <xsl:with-param name="string" select="@hash"/>
         </xsl:call-template>
      </xsl:variable>

      <!-- How many chunks of the same name -->
      <xsl:variable name="numberOfFollowingSiblings" select="count(following-sibling::aw:chunk[$currentName = @name])"/>
      <xsl:variable name="numberOfAlsoChunks" select="count(//aw:chunk[$currentName = @name])"/>

      <!--
         The next hash is formed with the chunkHashWithoutIndex with the current index+1 unless this is the last. Similar
         to previous hash variable
      -->
      <xsl:variable name="nextHash">
         <xsl:choose>
            <xsl:when test="$hashIndex = $numberOfAlsoChunks">
               <!-- We have to mark the end of the series -->
               <xsl:value-of select="'\relax'"/>
            </xsl:when>
            <xsl:otherwise>
               <!-- Mid chunks -->
               <xsl:value-of select="concat($chunkHashWithoutIndex, '-', $hashIndex + 1)"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>

      <xsl:variable name="previousHash">
         <xsl:choose>
            <xsl:when test="$hashIndex = 1">
               <!-- We have to mark the first of the series -->
               <xsl:value-of select="'\relax'"/>
            </xsl:when>
            <xsl:otherwise>
               <!-- There are more in the series so generate previous hash -->
               <xsl:value-of select="concat($chunkHashWithoutIndex, '-', $hashIndex - 1)"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>

      <!-- Only output if there are more than one chunk of this name -->
      <xsl:if test="not($numberOfAlsoChunks = 1)">
         <xsl:value-of select="concat('\nwprevnextdefs{', $previousHash, '}{', $nextHash, '}')"/>
      </xsl:if>

   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
       Mark sub label for indexing.
	-->

   <xsl:template name="sublabel">
      <xsl:text>\sublabel{</xsl:text>
      <xsl:value-of select="@hash"/>
      <xsl:text>}</xsl:text>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
       Mark sub label for indexing.
	-->

   <xsl:template name="nwmargintag">
      <xsl:text>\nwmargintag{</xsl:text>
      <xsl:call-template name="nwtagstyle"/>
      <xsl:text>}</xsl:text>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
       Mark sub label for indexing.
	-->

   <xsl:template name="nwtagstyle">
      <xsl:text>{\nwtagstyle{}\subpageref{</xsl:text>
      <xsl:value-of select="@hash"/>
      <xsl:text>}}</xsl:text>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	-->

   <xsl:template name="moddef">
      <xsl:text>\moddef{</xsl:text>
      <xsl:value-of disable-output-escaping="yes" select="@name"/>
      <xsl:if test="$index = 'yes'">
         <xsl:value-of select="$NON_BREAKING_SPACE"/>
         <xsl:call-template name="nwtagstyle"/>
      </xsl:if>
      <xsl:text>}</xsl:text>

      <xsl:variable name="hashIndex">
         <xsl:call-template name="substring-after-last">
            <xsl:with-param name="delimiter" select="'-'"/>
            <xsl:with-param name="string" select="@hash"/>
         </xsl:call-template>
      </xsl:variable>

      <xsl:choose>
         <xsl:when test="$hashIndex = 1">
            <xsl:value-of select="'\endmoddef'"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="'\plusendmoddef'"/>
         </xsl:otherwise>
      </xsl:choose>

   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	-->

   <xsl:template name="nwdocspar">
      <xsl:value-of select="concat('\nwdocspar', $NEW_LINE)"/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-* CHUNK AND VARIABLE INDEX -*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Output the sorted index of chunks and variables that were defined.
	-->

   <xsl:template name="output-final-index">
      <xsl:value-of select="concat($NEW_LINE, $NEW_LINE)"/>
      <xsl:call-template name="output-chunk-index"/>
      <xsl:call-template name="output-variable-index"/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	-->

   <xsl:template name="output-chunk-index">
      <xsl:for-each select="//aw:chunk[@type = 'code']">
         <xsl:sort select="@name" case-order="lower-first"/>
         <xsl:variable name="chunkName" select="@name"/>

         <!-- Final digits of hash for checking that there is no duplication-->
         <xsl:variable name="hashIndex">
            <xsl:call-template name="substring-after-last">
               <xsl:with-param name="delimiter" select="'-'"/>
               <xsl:with-param name="string" select="@hash"/>
            </xsl:call-template>
         </xsl:variable>

         <xsl:if test="$hashIndex = '1'">

            <!-- Prinary definition of chunk -->
            <xsl:value-of select="'\nwixlogsorted{c}{'"/>
            <xsl:value-of select="concat('{', @name, '}{', @hash, '}')"/>
            <xsl:value-of select="'{'"/>

            <!-- Scan each chunk for defined or used -->
            <xsl:for-each select="//aw:chunk[@type = 'code']">
               <xsl:variable name="scannedChunkName" select="@name"/>

               <xsl:choose>
                  <xsl:when test="$chunkName = $scannedChunkName">
                     <xsl:variable name="whereDefined" select="@hash"/>
                     <xsl:value-of select="concat('\nwixd{', $whereDefined, '}')"/>
                  </xsl:when>
                  <xsl:when test="$chunkName = descendant::aw:include/@ref">
                     <xsl:variable name="whereUsed" select="@hash"/>
                     <xsl:value-of select="concat('\nwixu{', $whereUsed, '}')"/>
                  </xsl:when>
               </xsl:choose>
            </xsl:for-each>
            <xsl:value-of select="concat('}}', $startCommentString, $NEW_LINE)"/>

         </xsl:if>

      </xsl:for-each>

   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	-->

   <xsl:template name="output-variable-index">
      <xsl:for-each select="//aw:definitions/aw:define">
         <xsl:sort select="@var" case-order="lower-first"/>

         <xsl:variable name="normalisedVar1">
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="@var"/>
            </xsl:call-template>
         </xsl:variable>

         <xsl:variable name="normalisedVar2">
            <xsl:call-template name="normalisedVarInIndex">
               <xsl:with-param name="text" select="@var"/>
            </xsl:call-template>
         </xsl:variable>

         <xsl:value-of select="concat('\nwixlogsorted{i}{{\nwixident{', $normalisedVar1, '}}{', $normalisedVar2, '}}', $startCommentString, $NEW_LINE)"/>
      </xsl:for-each>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- OUTPUT CODE LINES -*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	-->

   <xsl:template match="aw:code" mode="output-code">
      <xsl:apply-templates mode="output-line-fragment"/>
   </xsl:template>

   <xsl:template match="aw:token" mode="output-line-fragment">
      <xsl:variable name="normalisedTeXLine">
         <xsl:call-template name="normalisedTeXInCode">
            <xsl:with-param name="text" select="."/>
         </xsl:call-template>
      </xsl:variable>

      <xsl:value-of disable-output-escaping="yes" select="$normalisedTeXLine"/>
   </xsl:template>

   <xsl:template match="aw:separator" mode="output-line-fragment">
      <xsl:variable name="normalisedTeXLine">
         <xsl:call-template name="normalisedTeXInCode">
            <xsl:with-param name="text" select="."/>
         </xsl:call-template>
      </xsl:variable>

      <xsl:value-of disable-output-escaping="yes" select="$normalisedTeXLine"/>
   </xsl:template>

   <xsl:template match="aw:variable" mode="output-line-fragment">
      <xsl:variable name="normalisedTeXLine">
         <xsl:call-template name="normalisedTeXInDocumentation">
            <xsl:with-param name="text" select="."/>
         </xsl:call-template>
      </xsl:variable>

      <xsl:value-of select="concat('\nwlinkedidentc{', $normalisedTeXLine, '}{', ancestor::aw:chunk/@hash, '}')"/>
   </xsl:template>

   <xsl:template match="aw:include" mode="output-line-fragment">
      <xsl:variable name="thisIncludeRef" select="@ref"/>
      <xsl:variable name="chunkHash" select="//aw:chunk[@type = 'code'][@name = $thisIncludeRef]/@hash"/>
      <xsl:value-of select="'\LA{}'"/>
      <xsl:value-of select="concat($thisIncludeRef, $NON_BREAKING_SPACE)"/>
      <xsl:value-of select="concat('{\nwtagstyle{}\subpageref{', $chunkHash, '}}')"/>
      <xsl:value-of select="'\RA{}'"/>
      <xsl:value-of disable-output-escaping="yes" select="."/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Replace strings with correct TeX in code sections.
      
      @param text the string to process.

      @return the return value is the new string
   -->

   <xsl:template name="normalisedTeXInCode">
      <xsl:param name="text"/>
      <xsl:choose>
         <xsl:when test="contains($text, '$')">
            <xsl:value-of select="substring-before($text, '$')"/>
            <xsl:text>\&#x24;</xsl:text>
            <xsl:call-template name="normalisedTeXInCode">
               <xsl:with-param name="text" select="substring-after($text, '$')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains( $text, '&amp;' )">
            <xsl:value-of select="substring-before( $text, '&amp;' )"/>
            <xsl:text>\&amp;</xsl:text>
            <xsl:call-template name="normalisedTeXInCode">
               <xsl:with-param name="text" select="substring-after( $text, '&amp;' )"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($text, '%')">
            <xsl:value-of select="substring-before($text, '%')"/>
            <xsl:text>\%</xsl:text>
            <xsl:call-template name="normalisedTeXInCode">
               <xsl:with-param name="text" select="substring-after($text, '%')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($text, '#')">
            <xsl:value-of select="substring-before($text, '#')"/>
            <xsl:text>\#</xsl:text>
            <xsl:call-template name="normalisedTeXInCode">
               <xsl:with-param name="text" select="substring-after($text, '#')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($text, '_')">
            <xsl:value-of select="substring-before($text, '_')"/>
            <xsl:text>{\_}</xsl:text>
            <xsl:call-template name="normalisedTeXInCode">
               <xsl:with-param name="text" select="substring-after($text, '_')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($text, '{')">
            <xsl:value-of select="substring-before($text, '{')"/>
            <xsl:text>\{</xsl:text>
            <xsl:call-template name="normalisedTeXInCode">
               <xsl:with-param name="text" select="substring-after($text, '{')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($text, '}')">
            <xsl:value-of select="substring-before($text, '}')"/>
            <xsl:text>\}</xsl:text>
            <xsl:call-template name="normalisedTeXInCode">
               <xsl:with-param name="text" select="substring-after($text, '}')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($text, '\')">
            <xsl:value-of select="substring-before($text, '\')"/>
            <xsl:text>\\</xsl:text>
            <xsl:call-template name="normalisedTeXInCode">
               <xsl:with-param name="text" select="substring-after($text, '\')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$text"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*- OUTPUT DOCUMENTATION LINES *-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
 	-->

   <xsl:template name="output-documentation-lines">
      <xsl:variable name="chunkNumber" select="position()"/>
      <xsl:apply-templates mode="output-documentation-fragment"/>
   </xsl:template>

   <xsl:template match="aw:text" mode="output-documentation-fragment">

      <xsl:variable name="removeStart">
         <xsl:choose>
            <xsl:when test="starts-with(., '&lt;![CDATA[')">
               <xsl:value-of select="substring-after(., '&lt;![CDATA[')"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="."/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>

      <xsl:variable name="removeEnd">
         <xsl:choose>
            <xsl:when test="contains($removeStart, ']]&gt;') and not(substring-after($removeStart, ']]&gt;'))">
               <xsl:value-of select="substring-before($removeStart, ']]&gt;')"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="$removeStart"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>


      <xsl:variable name="normalisedTeXLine">
         <xsl:call-template name="normalisedTeXInDocumentation">
            <xsl:with-param name="text" select="$removeEnd"/>
         </xsl:call-template>
      </xsl:variable>

      <xsl:value-of disable-output-escaping="yes" select="$normalisedTeXLine"/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Replace strings with correct TeX in documentation sections.
      
      @param  text the string to process.
      @param  isInBracket is the previously translated character a bracket pair start.

      @return the return value is the new string
   -->

   <xsl:template name="normalisedTeXInDocumentation">
      <xsl:param name="text"/>
      <xsl:param name="isInBracket" select="false()"/>

      <xsl:choose>
         <xsl:when test="contains($text, '[[') and not($isInBracket)">
            <xsl:value-of select="substring-before($text, '[[')"/>
            <xsl:text>{\Tt{}</xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, '[[')"/>
               <xsl:with-param name="isInBracket" select="true()"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($text, ']]')">
            <xsl:value-of select="substring-before($text, ']]')"/>
            <xsl:text>\nwendquote}</xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, ']]')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($text, '$')">
            <xsl:value-of select="substring-before($text, '$')"/>
            <xsl:text>{\&#x24;}</xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, '$')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($text, '&#x2014;')">
            <xsl:value-of select="substring-before($text, '&#x2014;')"/>
            <xsl:text>---</xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, '&#x2014;')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains( $text, '&amp;' )">
            <xsl:value-of select="substring-before( $text, '&amp;' )"/>
            <xsl:text>{\&amp;}</xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after( $text, '&amp;' )"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($text, '%')">
            <xsl:value-of select="substring-before($text, '%')"/>
            <xsl:text>{\%}</xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, '%')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($text, '#')">
            <xsl:value-of select="substring-before($text, '#')"/>
            <xsl:text>{\#}</xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, '#')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($text, '_')">
            <xsl:value-of select="substring-before($text, '_')"/>
            <xsl:text>{\_}</xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, '_')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$text"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Output ident refs at the end of a code chunk.
	-->

   <xsl:template name="outputIdentRefs">
      <xsl:param name="inputText" select="."/>
      <xsl:param name="delimiter" select="$SPACE"/>
      <xsl:param name="startText" select="'\\{'"/>
      <xsl:param name="endText" select="'}'"/>

      <xsl:if test="string-length($inputText) > 0">
         <xsl:variable name="nextItem" select="substring-before(concat($inputText, $delimiter), $delimiter)"/>
         <xsl:variable name="chunkHash" select="//aw:chunk[@id = $nextItem]/@hash"/>

         <xsl:value-of select="concat($startText, $chunkHash, $endText)"/>
         <xsl:call-template name="outputIdentRefs">
            <xsl:with-param name="inputText" select="substring-after($inputText, $delimiter)"/>
            <xsl:with-param name="delimiter" select="$delimiter"/>
            <xsl:with-param name="startText" select="$startText"/>
            <xsl:with-param name="endText" select="$endText"/>
         </xsl:call-template>
      </xsl:if>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Normalise variable string for index.
      
      Needs expanding.
      
      @param text the string to process.

      @return the return value is the new string
   -->

   <xsl:template name="normalisedVarInIndex">
      <xsl:param name="text"/>
      <xsl:choose>
         <xsl:when test="contains($text, '$')">
            <xsl:value-of select="substring-before($text, '$')"/>
            <xsl:text>:do</xsl:text>
            <xsl:call-template name="normalisedVarInIndex">
               <xsl:with-param name="text" select="substring-after($text, '$')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains( $text, '&amp;' )">
            <xsl:value-of select="substring-before( $text, '&amp;' )"/>
            <xsl:text>:am</xsl:text>
            <xsl:call-template name="normalisedVarInIndex">
               <xsl:with-param name="text" select="substring-after( $text, '&amp;' )"/>
            </xsl:call-template>
         </xsl:when>

         <xsl:otherwise>
            <xsl:value-of select="$text"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-* UTILITY FUNCTIONS *-*-*-*-*-*-*-*-*-*-*-*-*-*- -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Extract string after last delimiter.
      
      @param string string the input string
      @param string delimiter the delimiter character

      @return the return value is the extracted string
	-->

   <xsl:template name="substring-after-last">
      <xsl:param name="string"/>
      <xsl:param name="delimiter"/>
      <xsl:choose>
         <xsl:when test="contains($string, $delimiter)">
            <xsl:call-template name="substring-after-last">
               <xsl:with-param name="string" select="substring-after($string, $delimiter)"/>
               <xsl:with-param name="delimiter" select="$delimiter"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$string"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Extract string before last delimiter.
      
      @param string string the input string
      @param string delimiter the delimiter character

      @return the return value is the extracted string
	-->

   <xsl:template name="substring-before-last">
      <xsl:param name="string"/>
      <xsl:param name="delimiter"/>
      <xsl:choose>
         <xsl:when test="contains($string, $delimiter)">
            <!-- get everything in front of the first delimiter -->
            <xsl:value-of select="substring-before($string, $delimiter)"/>
            <xsl:choose>
               <xsl:when test="contains(substring-after($string, $delimiter), $delimiter)">
                  <xsl:value-of select="$delimiter"/>
               </xsl:when>
            </xsl:choose>
            <xsl:call-template name="substring-before-last">
               <!-- store anything left in another variable -->
               <xsl:with-param name="string" select="substring-after($string, $delimiter)"/>
               <xsl:with-param name="delimiter" select="$delimiter"/>
            </xsl:call-template>
         </xsl:when>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Remove duplicate tokens from string.
      
      @param string string the string from which tokens are processed
      @param string newstring internal recursion paramter for building new string
      @param string delimiter the character that separates the tokens

      @return the return value is the new string
   -->

   <xsl:template name="remove-duplicates">
      <xsl:param name="string"/>
      <xsl:param name="newstring"/>
      <xsl:param name="delimiter"/>

      <xsl:choose>
         <!--
            When string being processed is empty we have finished so the new string
            should contain what we are interested in.
         -->
         <xsl:when test="$string = ''">
            <xsl:value-of select="$newstring"/>
         </xsl:when>

         <!--
            Still things to process
         -->
         <xsl:otherwise>
            <xsl:variable name="nextToken" select="substring-before($string, $delimiter)"/>
            <xsl:variable name="remainderLine" select="substring-after($string, $delimiter)"/>

            <!-- Is the next token in the new string -->
            <xsl:choose>
               <xsl:when test="contains($newstring, $nextToken)">
                  <!-- Yes so remove it -->
                  <xsl:call-template name="remove-duplicates">
                     <xsl:with-param name="string" select="$remainderLine"/>
                     <xsl:with-param name="newstring" select="$newstring"/>
                     <xsl:with-param name="delimiter" select="$delimiter"/>
                  </xsl:call-template>
               </xsl:when>
               <xsl:otherwise>
                  <!-- Build new string into temporary variable -->
                  <xsl:variable name="temp">
                     <xsl:choose>
                        <xsl:when test="$newstring = ''">
                           <xsl:value-of select="$nextToken"/>
                        </xsl:when>
                        <xsl:otherwise>
                           <xsl:value-of select="concat($newstring, $delimiter, $nextToken)"/>
                        </xsl:otherwise>
                     </xsl:choose>
                  </xsl:variable>
                  <xsl:call-template name="remove-duplicates">
                     <xsl:with-param name="string" select="$remainderLine"/>
                     <xsl:with-param name="newstring" select="$temp"/>
                     <xsl:with-param name="delimiter" select="$delimiter"/>
                  </xsl:call-template>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:otherwise>
      </xsl:choose>

   </xsl:template>


</xsl:stylesheet>
