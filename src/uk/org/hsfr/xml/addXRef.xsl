<?xml version="1.0" encoding="UTF-8"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--

   Cross reference chunks.

   Author:
	   Name  : Hugh Field-Richards
	   Email : hsfr@hsfr.org.uk

-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   Date                   Who    Changes
   ==========================================================================
   18th June 2015         HSFR   Created

-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Copyright -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   All copyright, database rights and other intellectual property in this
   software is the property of Hugh Field-Richards. The software is being
   released free of charge for use in accordance with the terms of open
   source initiative GNU General Public Licence.
      
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* Disclaimer -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   The author and copyright holder does not verify or warrant the accuracy,
   completeness or suitability of this software for any particular purpose.
   Any use of this software whatsoever is entirely at the user's own risk
   and the author and copyright holder accepts no liability in relation thereto.
                  
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:aw="http://www.hsfr.org.uk/Schema/AntWeave" version="1.0">

   <xsl:output version="1.0" encoding="UTF-8"/>

   <xsl:preserve-space elements="*"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Global constants.
   -->

   <xsl:variable name="NEW_LINE" select="'&#xA;'"/>
   <xsl:variable name="NON_BREAKING_SPACE" select="'~'"/>
   <xsl:variable name="SPACE" select="' '"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xsl:template match="/">
      <xsl:element name="aw:file">
         <xsl:attribute name="filter">addXRef</xsl:attribute>
         <xsl:apply-templates mode="process-chunks-and-definitions"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Code chunk are passed through unchanged but we add where the chunks are used and 
      also the variable definitions (%def).
   -->

   <xsl:template match="aw:chunk[@type = 'code']" mode="process-chunks-and-definitions">
      <xsl:element name="aw:chunk">
         <xsl:copy-of select="@*"/>
         <xsl:apply-templates mode="output-code-chunk"/>
      </xsl:element>
      <xsl:value-of select="$NEW_LINE"/>
      <!-- Where this chunk is used -->
      <xsl:call-template name="output-where-chunk-used"/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Documentation chunk are passed through unchanged.
   -->

   <xsl:template match="aw:chunk[@type = 'documentation']" mode="process-chunks-and-definitions">
      <xsl:element name="aw:chunk">
         <xsl:copy-of select="@*"/>
         <xsl:value-of select="$NEW_LINE"/>
         <xsl:call-template name="pass-through"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	-->

   <xsl:template match="aw:code" mode="output-code-chunk">
      <xsl:element name="aw:code">
         <xsl:copy-of select="@*"/>
         <xsl:apply-templates mode="output-code-fragment"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	-->

   <xsl:template match="aw:include" mode="output-code-chunk">
      <xsl:element name="aw:include">
         <xsl:copy-of select="@*"/>
         <xsl:value-of select="."/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xsl:template match="aw:definitions" mode="process-chunks-and-definitions">
      <xsl:element name="aw:definitions">
         <xsl:copy-of select="@*"/>
         <xsl:apply-templates mode="output-where-variable-used"/>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xsl:template match="aw:definition" mode="output-where-variable-used">
      <xsl:variable name="variableName" select="."/>
      <xsl:variable name="associatedChunk" select="parent::*/@chunk"/>

      <xsl:element name="aw:define">
         <xsl:attribute name="var">
            <xsl:value-of select="$variableName"/>
         </xsl:attribute>

         <!--
            Cannot do this with variable keys. Scan each code chunk.
         -->
         <xsl:for-each select="//aw:chunk[@type = 'code']">
            <xsl:variable name="chunkNumber" select="@id"/>
            <xsl:variable name="chunkHash" select="@hash"/>

            <xsl:for-each select="aw:code">
               <xsl:for-each select="aw:token">
                  <xsl:variable name="contents" select="."/>
                  <xsl:choose>
                     <xsl:when test="contains($contents, $variableName) and not($chunkHash = $associatedChunk)">
                        <xsl:element name="aw:where">
                           <xsl:attribute name="ref">
                              <xsl:value-of select="$chunkNumber"/>
                           </xsl:attribute>
                        </xsl:element>
                     </xsl:when>
                  </xsl:choose>
               </xsl:for-each>
            </xsl:for-each>
         </xsl:for-each>
      </xsl:element>

   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
         We wrap the usedIn tag inside its own definitions tag to help the index filter.
    -->

   <xsl:template name="output-where-chunk-used">

      <xsl:variable name="currentChunkRef" select="@name"/>

      <xsl:element name="aw:definitions">
         <xsl:attribute name="chunk">
            <xsl:value-of select="@hash"/>
         </xsl:attribute>

         <xsl:element name="aw:usedIn">
            <!-- scan all code chunks for this chunk being used -->
            <xsl:for-each select="//aw:chunk[@type = 'code']">
               <!-- Use the id not the hash here -->
               <xsl:variable name="chunkNumber" select="@id"/>

               <!-- scan all includes in the code for the current chunk being used -->
               <xsl:for-each select="aw:code/aw:include">
                  <xsl:if test="@ref = $currentChunkRef">
                     <xsl:element name="aw:where">
                        <xsl:attribute name="ref">
                           <xsl:value-of select="$chunkNumber"/>
                        </xsl:attribute>
                     </xsl:element>
                  </xsl:if>
               </xsl:for-each>
            </xsl:for-each>
         </xsl:element>

      </xsl:element>

   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	-->

   <xsl:template match="text()" mode="output-code-fragment">
      <!--
         It is important to have disable-output-escaping set here to make
         sure that the line is copied verbatim.
      -->
      <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
      <xsl:value-of disable-output-escaping="yes" select="."/>
      <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
   </xsl:template>

   <xsl:template match="aw:token" mode="output-code-fragment">
      <xsl:element name="aw:token">
         <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
         <xsl:value-of disable-output-escaping="yes" select="."/>
         <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
      </xsl:element>
   </xsl:template>

   <xsl:template match="aw:separator" mode="output-code-fragment">
      <xsl:element name="aw:separator">
         <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
         <xsl:value-of disable-output-escaping="yes" select="."/>
         <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
      </xsl:element>
   </xsl:template>

   <xsl:template match="aw:include" mode="output-code-fragment">
      <xsl:copy-of select="@*"/>
      <xsl:copy-of select="."/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
	-->

   <xsl:template name="pass-through">
      <xsl:for-each select="aw:text">
         <xsl:element name="aw:text">
            <!-- important to have disable-output-escaping set here -->
            <xsl:copy-of select="@*"/>
            <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
            <xsl:value-of disable-output-escaping="yes" select="."/>
            <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
         </xsl:element>
         <xsl:value-of select="$NEW_LINE"/>
      </xsl:for-each>
   </xsl:template>

</xsl:stylesheet>
