<?xml version="1.0" encoding="UTF-8"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--

   Tagged noweb to tangle Transform

   Author:
	   Name  : Hugh Field-Richards
	   Email : hsfr@hsfr.org.uk

-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   Date                   Who    Changes
   ==========================================================================
   10th June 2015         HSFR   Created

-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Copyright -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   All copyright, database rights and other intellectual property in this
   software is the property of Hugh Field-Richards. The software is being
   released free of charge for use in accordance with the terms of open
   source initiative GNU General Public Licence.
      
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* Disclaimer -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   The author and copyright holder does not verify or warrant the accuracy,
   completeness or suitability of this software for any particular purpose.
   Any use of this software whatsoever is entirely at the user's own risk
   and the author and copyright holder accepts no liability in relation thereto.
                  
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:aw="http://www.hsfr.org.uk/Schema/AntWeave" version="1.0">

   <xsl:output method="text" version="1.0" omit-xml-declaration="yes" encoding="UTF-8" indent="no"/>

   <xsl:preserve-space elements="aw:code"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Add any parameters from the calling program here.
   -->

   <xsl:param name="rootChunk"/>
   <xsl:param name="startCommentString" select="'//'"/>
   <xsl:param name="endCommentString" select="''"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xsl:template match="/">
      <!-- Find the first code chunk to process (and all others with same name) -->
      <xsl:for-each select="//aw:chunk[(@name = '*' or @name = $rootChunk) and @type = 'code']">

         <xsl:variable name="rootName">
            <xsl:choose>
               <xsl:when test="string-length($rootChunk) = 0">
                  <xsl:value-of select="'*'"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="$rootChunk"/>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:variable>

         <xsl:variable name="firstChunkPosition" select="position()"/>

         <xsl:call-template name="process-code">
            <xsl:with-param name="isFirstChunk" select="$firstChunkPosition = 1"/>
            <xsl:with-param name="chunkNode" select="."/>
            <xsl:with-param name="name" select="$rootName"/>
         </xsl:call-template>

      </xsl:for-each>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Process a code chunk.
      
      @param boolean isFirstChunk is this first chunk of code (used to inhbit first comment in XML files)
      @param nodes chunkNode the current node chunk being processed (a list of code lines)
      @param string name the name of the current code chunk

      @return the raw code line to be output
	-->

   <xsl:template name="process-code">
      <xsl:param name="isFirstChunk"/>
      <xsl:param name="chunkNode"/>
      <xsl:param name="name"/>

      <xsl:for-each select="$chunkNode">
         <xsl:if test="not($isFirstChunk)">
            <xsl:value-of select="$startCommentString"/>
            <xsl:text> File:</xsl:text>
            <xsl:value-of select="@file"/>
            <xsl:text> Line:</xsl:text>
            <xsl:value-of select="@line"/>
            <xsl:text> chunk:</xsl:text>
            <xsl:value-of select="$name"/>
            <xsl:value-of select="$endCommentString"/>
            <xsl:text>&#x0A;</xsl:text>
         </xsl:if>

         <!-- For each of the code lines in this chunk -->
         <xsl:for-each select="aw:code">
            <!-- For each of the tokens and separator elements in this line -->
            <xsl:for-each select="*">
               <xsl:choose>
                  <xsl:when test="name() = 'aw:include'">
                     <!-- An include statement which defines a reference to another chunk which must be inserted here -->
                     <xsl:variable name="chunkRef" select="@ref"/>
                     <!-- Recurse until all included lines are output -->
                     <xsl:call-template name="process-code">
                        <!-- A set of nodes that are the named chunks -->
                        <xsl:with-param name="chunkNode" select="//aw:chunk[@name = $chunkRef]"/>
                        <xsl:with-param name="name" select="$chunkRef"/>
                     </xsl:call-template>
                  </xsl:when>
                  <xsl:otherwise>
                     <!-- Ordinary text so just output it -->
                     <xsl:value-of disable-output-escaping="yes" select="."/>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:for-each>
            <xsl:text>&#x0A;</xsl:text>
         </xsl:for-each>
      </xsl:for-each>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
       Soak up any remaining elements not processed by the above.
                     <xsl:call-template name="process-code">
                        <xsl:with-param name="chunkNode" select="//aw:chunk[@name = $chunkRef]"/>
                        <xsl:with-param name="currentFile" select="//aw:chunk[@name = $chunkRef][position() = last()]/@file"/>
                        <xsl:with-param name="lineNumber" select="//aw:chunk[@name = $chunkRef][position() = last()]/@line"/>
                        <xsl:with-param name="name" select="//aw:chunk[@name = $chunkRef][position() = last()]/@name"/>
                     </xsl:call-template>

   
   -->

   <xsl:template match="node() | @*" priority="-1"> </xsl:template>


</xsl:stylesheet>
