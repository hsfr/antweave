<?xml version="1.0" encoding="UTF-8"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--

  HTML Transform

   Author:
	   Name  : Hugh Field-Richards
	   Email : hsfr@hsfr.org.uk

-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   Date                   Who    Changes
   ==========================================================================
   14th July 2015         HSFR   Created

-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- Copyright -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   All copyright, database rights and other intellectual property in this
   software is the property of Hugh Field-Richards. The software is being
   released free of charge for use in accordance with the terms of open
   source initiative GNU General Public Licence.
      
-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* Disclaimer -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   The author and copyright holder does not verify or warrant the accuracy,
   completeness or suitability of this software for any particular purpose.
   Any use of this software whatsoever is entirely at the user's own risk
   and the author and copyright holder accepts no liability in relation thereto.
                  
-->

<!DOCTYPE xsl:stylesheet [
   <!-- Used in annotation -->
   <!ENTITY lt       "&#38;#60;" >     <!-- less-than sign -->
   <!ENTITY gt       "&#62;" >         <!-- greater-than sign -->
   <!ENTITY dash     "&#x2010;" >      <!-- hyphen (true graphic) -->
   <!ENTITY ndash    "&#x2013;" >      <!-- en dash -->
   <!ENTITY mdash    "&#x2014;" >      <!-- em dash -->
   <!ENTITY ldquo    "&#x201c;" >      <!-- Left Double Quote -->
   <!ENTITY rdquo    "&#x201d;" >      <!-- Right Double Quote -->
   <!ENTITY lsquo    "&#x2018;" >      <!-- Left Single Quote -->
   <!ENTITY rsquo    "&#x2019;" >      <!-- Right Single Quote -->
   <!ENTITY bull     "&#x2022;" >      <!-- round bullet, filled -->
   <!ENTITY oslash   "&#248;" >        <!-- small o, slash -->
   <!ENTITY ocirc    "&#244;" >        <!-- small o, circumflex accent -->
   <!ENTITY nbsp     "&#160;" >        <!-- non breaking space -->

] >


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml"
   xmlns:aw="http://www.hsfr.org.uk/Schema/AntWeave" version="1.0">

   <xsl:output method="xml" version="1.0" omit-xml-declaration="no" encoding="UTF-8" indent="yes"
      doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Add any parameters from the calling program here.
   -->

   <xsl:param name="antweaveFileName" select="''"/>
   <xsl:param name="startCommentString" select="'&lt;!-- '"/>
   <xsl:param name="endCommentString" select="' -->'"/>
   <xsl:param name="delay" select="'no'"/>
   <xsl:param name="index" select="'yes'"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
      Global constants.
   -->

   <xsl:variable name="NEW_LINE" select="'&#xA;'"/>
   <xsl:variable name="NON_BREAKING_SPACE" select="'~'"/>
   <xsl:variable name="SPACE" select="' '"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--
   -->

   <xhtml:h2>Global Variables</xhtml:h2>

   <xhtml:p>Comma separated list of style files. But the styles files use a semicolon (don't ask!). </xhtml:p>

   <xhtml:p>Path to CSS files directory.</xhtml:p>
   <xsl:variable name="gCSSFilesDir" select="''"/>
   <xhtml:p>Comma separated list of CSS files.</xhtml:p>
   <xsl:variable name="gCSSFiles" select="'noweb.css'"/>

   <xhtml:p>Path to script files directory.</xhtml:p>
   <xsl:variable name="gScriptFilesDir" select="''"/>
   <xhtml:p>Semicolon separated list of script files.</xhtml:p>
   <xsl:variable name="gScriptFiles" select="''"/>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!--  
      <xsl:call-template name="process-chunks"/>
   -->

   <xsl:template match="/">

      <xsl:element name="html">
         <xsl:element name="head">
            <xsl:call-template name="buildHeader.htmlHead">
               <xsl:with-param name="inTitle" select="$antweaveFileName"/>
               <xsl:with-param name="inStylesList" select="$gCSSFiles"/>
               <xsl:with-param name="inStylesDir" select="$gCSSFilesDir"/>
               <xsl:with-param name="inScriptsList" select="$gScriptFiles"/>
               <xsl:with-param name="inScriptsDir" select="$gScriptFilesDir"/>
            </xsl:call-template>
         </xsl:element>
         <xsl:element name="body">
            <xsl:element name="div">
               <xsl:attribute name="id">bodyPanel</xsl:attribute>
               <xsl:call-template name="process-body"/>
            </xsl:element>
         </xsl:element>

      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
     param inTitle the title of the page
     param inStylesList the list of style files to output (file-1,file-2,file-3, ...)
   -->

   <xsl:template name="buildHeader.htmlHead">
      <xsl:param name="inTitle"/>
      <xsl:param name="inStylesList"/>
      <xsl:param name="inStylesDir"/>
      <xsl:param name="inScriptsList"/>
      <xsl:param name="inScriptsDir"/>

      <xsl:element name="meta">
         <xsl:attribute name="name">DC.creator</xsl:attribute>
         <xsl:attribute name="content">Hugh Field-Richards</xsl:attribute>
      </xsl:element>

      <xsl:element name="meta">
         <xsl:attribute name="name">DC.language</xsl:attribute>
         <xsl:attribute name="scheme">RFC1766</xsl:attribute>
         <xsl:attribute name="content">en</xsl:attribute>
      </xsl:element>

      <xsl:call-template name="buildHeader.outputStylesList">
         <xsl:with-param name="inStylesList" select="$inStylesList"/>
         <xsl:with-param name="inStylesDir" select="$inStylesDir"/>
      </xsl:call-template>

      <xsl:element name="title">
         <xsl:value-of select="$inTitle"/>
      </xsl:element>

   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
   -->

   <xsl:template name="process-body">
      <xsl:for-each select="//aw:chunk">
         <xsl:choose>
            <xsl:when test="@type = 'documentation'">
               <xsl:if test="position() = last()">
                  <xsl:call-template name="output-final-index"/>
               </xsl:if>
               <xsl:element name="div">
                  <xsl:attribute name="class">documentationPanel</xsl:attribute>
                  <xsl:call-template name="output-documentation-lines"/>
               </xsl:element>
            </xsl:when>
            <xsl:when test="@type = 'code'">
               <xsl:call-template name="output-code-lines"/>
            </xsl:when>
         </xsl:choose>
      </xsl:for-each>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xsl:template name="output-documentation-lines">
      <xsl:variable name="chunkNumber" select="position()"/>
      <xsl:apply-templates mode="output-documentation-fragment"/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* CODE CHUNK -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xsl:template name="output-code-lines">
      <xsl:element name="div">
         <xsl:attribute name="class">codeInChunk</xsl:attribute>
         <!-- Output the name of this chunk -->
         <xsl:element name="div">
            <xsl:attribute name="class">chunkTitle</xsl:attribute>
            <xsl:value-of select="'&lt;&lt;'"/>
            <xsl:value-of disable-output-escaping="yes" select="@name"/>
            <xsl:value-of select="'>>='"/>
         </xsl:element>

         <xsl:element name="div">
            <xsl:attribute name="class">codeLines</xsl:attribute>
            <xsl:apply-templates mode="output-line-fragment"/>
         </xsl:element>
      </xsl:element>
   </xsl:template>

   <!--
      -->

   <xsl:template match="aw:token | aw:separator" mode="output-line-fragment">
      <xsl:variable name="normalisedTeXLine">
         <xsl:call-template name="normaliseTeXInCode">
            <xsl:with-param name="text" select="."/>
         </xsl:call-template>
      </xsl:variable>

      <xsl:value-of disable-output-escaping="yes" select="$normalisedTeXLine"/>
   </xsl:template>

   <xsl:template match="aw:variable" mode="output-line-fragment">
      <xsl:variable name="normalisedTeXLine">
         <xsl:call-template name="normaliseTeXInCode">
            <xsl:with-param name="text" select="."/>
         </xsl:call-template>
      </xsl:variable>

      <xsl:value-of disable-output-escaping="yes" select="$normalisedTeXLine"/>
   </xsl:template>

   <xsl:template match="aw:include" mode="output-line-fragment">
      <xsl:variable name="thisIncludeRef" select="@ref"/>
      <xsl:variable name="chunkHash" select="//aw:chunk[@type = 'code'][@name = $thisIncludeRef]/@hash"/>
      <xsl:value-of select="'&lt;&lt;'"/>
      <xsl:value-of disable-output-escaping="yes" select="@ref"/>
      <xsl:value-of select="'>>'"/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Replace strings with correct HTML in code sections.
      
      @param string text the string to process.

      @return the return value is the new string
   -->

   <xsl:template name="normaliseTeXInCode">
      <xsl:param name="text"/>
      <xsl:choose>
         <xsl:when test="contains( $text, '&amp;' )">
            <xsl:value-of select="substring-before( $text, '&amp;' )"/>
            <xsl:text>\&amp;</xsl:text>
            <xsl:call-template name="normaliseTeXInCode">
               <xsl:with-param name="text" select="substring-after( $text, '&amp;' )"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$text"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>


   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xsl:template match="aw:text" mode="output-documentation-fragment">

      <xsl:variable name="removeStart">
         <xsl:choose>
            <xsl:when test="starts-with(., '&lt;![CDATA[')">
               <xsl:value-of select="substring-after(., '&lt;![CDATA[')"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="."/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>

      <xsl:variable name="removeEnd">
         <xsl:choose>
            <xsl:when test="contains($removeStart, ']]&gt;') and not(substring-after($removeStart, ']]&gt;'))">
               <xsl:value-of select="substring-before($removeStart, ']]&gt;')"/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="$removeStart"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>

      <xsl:variable name="normalisedTeXLine">
         <xsl:call-template name="normalisedTeXInDocumentation">
            <xsl:with-param name="text" select="$removeEnd"/>
            <xsl:with-param name="currentTag" select="''"/>
         </xsl:call-template>
      </xsl:variable>
      <xsl:value-of disable-output-escaping="yes" select="$normalisedTeXLine"/>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Replace strings with correct TeX in documentation sections.
      
      @param string text the string to process.
      @param string currentTag the current tag being processed.

      @return the return value is the new string
   -->

   <xsl:template name="normalisedTeXInDocumentation">
      <xsl:param name="text"/>
      <xsl:param name="currentTag"/>

      <xsl:choose>
         <xsl:when test="starts-with($text, '%')">
         </xsl:when>
         
         <xsl:when test="contains($text, '``')">
            <xsl:value-of select="substring-before($text, '``')"/>
            <xsl:text>&rsquo;</xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, '``')"/>
            </xsl:call-template>
         </xsl:when>
         
         <xsl:when test="contains($text, '&rsquo;&rsquo;')">
            <xsl:value-of select="substring-before($text, '&rsquo;&rsquo;')"/>
            <xsl:text>”</xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, '&rsquo;&rsquo;')"/>
            </xsl:call-template>
         </xsl:when>

         <xsl:when test="contains($text, '[[')">
            <xsl:value-of select="substring-before($text, '[[')"/>
            <xsl:text>&lt;span class="documentationSnippet"></xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, '[[')"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($text, ']]')">
            <xsl:value-of select="substring-before($text, ']]')"/>
            <xsl:text>&lt;/span></xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, ']]')"/>
            </xsl:call-template>
         </xsl:when>

         <xsl:when test="contains($text, '\section{')">
            <xsl:value-of select="substring-before($text, '\section{')"/>
            <xsl:text>&lt;div class="sectionHeading"></xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, '\section{')"/>
               <xsl:with-param name="currentTag" select="'div'"/>
            </xsl:call-template>
         </xsl:when>

         <xsl:when test="contains($text, '\subsection{')">
            <xsl:value-of select="substring-before($text, '\subsection{')"/>
            <xsl:text>&lt;div class="subsectionHeading"></xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, '\subsection{')"/>
               <xsl:with-param name="currentTag" select="'div'"/>
            </xsl:call-template>
         </xsl:when>

          <xsl:when test="contains($text, '\subsubsection{')">
            <xsl:value-of select="substring-before($text, '\subsubsection{')"/>
            <xsl:text>&lt;div class="subsubsectionHeading"></xsl:text>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, '\subsubsection{')"/>
               <xsl:with-param name="currentTag" select="'div'"/>
            </xsl:call-template>
         </xsl:when>

         <xsl:when test="contains($text, '}')">
            <xsl:value-of select="substring-before($text, '}')"/>
            <xsl:value-of select="concat('&lt;/', $currentTag, '>')"/>
            <xsl:call-template name="normalisedTeXInDocumentation">
               <xsl:with-param name="text" select="substring-after($text, '}')"/>
               <xsl:with-param name="currentTag" select="$currentTag"/>
            </xsl:call-template>
         </xsl:when>

         <xsl:otherwise>
            <xsl:value-of select="$text"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Output the sorted index of chunks and variables that were defined.
	-->

   <xsl:template name="output-final-index">
      <xsl:element name="div">
         <xsl:attribute name="id">indexPanel</xsl:attribute>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Output a list of comma separated style files as
      
      <link href="file-1" ... />
      <link href="file-2" ... />
      <link href="file-3" ... />
      
      param inStylesList the list of style files to output (file-1,file-2,file-3, ...)
   -->

   <xsl:template name="buildHeader.outputStylesList">
      <xsl:param name="inStylesList"/>
      <xsl:param name="inStylesDir"/>

      <xsl:choose>
         <xsl:when test="contains($inStylesList, ',')">
            <xsl:variable name="firstStyle" select="substring-before($inStylesList, ',')"/>
            <xsl:variable name="remainderStyles" select="substring-after($inStylesList, ',')"/>
            <xsl:call-template name="buildHeader.outputStyle">
               <xsl:with-param name="inStyle" select="$firstStyle"/>
               <xsl:with-param name="inStylesDir" select="$inStylesDir"/>
            </xsl:call-template>
            <xsl:call-template name="buildHeader.outputStylesList">
               <xsl:with-param name="inStylesList" select="$remainderStyles"/>
               <xsl:with-param name="inStylesDir" select="$inStylesDir"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="buildHeader.outputStyle">
               <xsl:with-param name="inStyle" select="$inStylesList"/>
               <xsl:with-param name="inStylesDir" select="$inStylesDir"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Output a single style files as <link href="inStyle" ... />
      
      param inStyle the styles file name
   -->

   <xsl:template name="buildHeader.outputStyle">
      <xsl:param name="inStyle"/>
      <xsl:param name="inStylesDir"/>
      <xsl:element name="link">
         <xsl:attribute name="href">
            <xsl:value-of select="concat($inStylesDir, $inStyle)"/>
         </xsl:attribute>
         <xsl:attribute name="rel">stylesheet</xsl:attribute>
         <xsl:attribute name="type">text/css</xsl:attribute>
      </xsl:element>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->

   <xsl:template name="buildHeader.outputScriptsList">
      <xsl:param name="inScriptsList"/>
      <xsl:param name="inScriptsDir"/>

      <xsl:choose>
         <xsl:when test="contains($inScriptsList, ';')">
            <xsl:variable name="firstScript" select="substring-before($inScriptsList, ';')"/>
            <xsl:variable name="remainderScripts" select="substring-after($inScriptsList, ';')"/>
            <xsl:call-template name="buildHeader.outputScript">
               <xsl:with-param name="inScript" select="$firstScript"/>
               <xsl:with-param name="inScriptsDir" select="$inScriptsDir"/>
            </xsl:call-template>
            <xsl:call-template name="buildHeader.outputScriptsList">
               <xsl:with-param name="inScriptsList" select="$remainderScripts"/>
               <xsl:with-param name="inScriptsDir" select="$inScriptsDir"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="buildHeader.outputScript">
               <xsl:with-param name="inScript" select="$inScriptsList"/>
               <xsl:with-param name="inScriptsDir" select="$inScriptsDir"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
   <!-- 
      Output a single script files as <script type="text/javascript" src="js/prototype.js" ... />
   -->

   <xsl:template name="buildHeader.outputScript">
      <xsl:param name="inScript"/>
      <xsl:param name="inScriptsDir"/>

      <xsl:element name="script">
         <xsl:attribute name="src">
            <xsl:value-of select="concat($inScriptsDir, $inScript)"/>
         </xsl:attribute>
         <xsl:attribute name="type">text/javascript</xsl:attribute>
      </xsl:element>
   </xsl:template>


</xsl:stylesheet>
